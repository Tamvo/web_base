<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
  protected $table ='product_detail';
  protected $primaryKey = 'id';
  public $timestamps=true;
  protected $fillable = ['id','id_product','picture','created_at','update_at'];

  public function getItems()
  {
    return $this->orderBy('id','DESC')->paginate(5);
  }

  public function product()
  {
    return $this->belongsTo('App\Product');
  }

  public function getItem($id)
  {
    return $this->findOrFail($id);
  }

  public function getIdPro($id)
  {
    return $this->where('id_product', $id)->latest('id')->first();
  }

  public function getIdPros($id)
  {
    return $this->where('id_product', $id)->get();
  }

  public function addItem($arItem)
  {
    return $this->insert($arItem);
  }

  public function editItems($id, $arItem)
  {
    $oItem = $this->findOrFail($id);

    return $oItem->update($arItem);
  }

  public function deleteItem($id)
  {
    $oItem = $this->findOrFail($id);

    return $oItem->delete();
  }

  public function deletePro($id)
  {
    $oItem = $this->where('id_product',$id);
    
    return $oItem->delete();
  }
}
