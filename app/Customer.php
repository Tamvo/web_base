<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	protected $table ='customer';
  protected $primaryKey = 'id';
  public $timestamps=true;
  protected $fillable = ['id','username','fullname','password','created_at','updated_at'];

  public function getItems()
  {
    return $this->orderBy('id','DESC')->paginate(5);
  }

  public function getItem($id)
  {
    return $this->findOrFail($id);
  }

  public function getId($id)
  {
    return $this->find($id);
  }

  public function getIdType($id)
  {
    return $this->where('id_cat', $id)->get();
  }

  public function addItem($arItem)
  {
    return $this->insert($arItem);
  }

  public function editItems($id, $arItem)
  {
    $oItem = $this->findOrFail($id);
    return $oItem->update($arItem);
  }

  public function deleteItem($id)
  {
    $oItem = $this->findOrFail($id);
    return $oItem->delete();
  }
}
