<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
	protected $table = 'bills';
	protected $primaryKey = 'id';
	public $timestamps = true;
	protected $fillable = ['id','id_user', 'date_order','total_price','active', 'created_at','updated_at'];

	public function getItems()
	{
		return $this->join('users','bills.id_user','=','users.id')
		// ->join('customer_detail', 'customer.id','=','customer_detail.id_user')
		->select('bills.*', 'users.fullname')
		->orderBy('id','DESC')
		->paginate(5);
	}

	public function getBill($id)
	{
		return $this->where('id_user', $id)->get();
	}

	public function billDetails()
	{
		return $this->hasMany('App\BillDetail', 'id_bill');
	}
	
	public function getItem($id)
	{
		return $this->findOrFail($id);
	}

	public function getId($id)
	{
		return $this->find($id);
	}


	public function active($id)
	{
		$oItem =  $this->findOrFail($id);
		if( $oItem->active ==0){
			$oItem->active =1;
			return $oItem->save();
		}
	}

	public function deleteItem($id)
	{
		$oItem = $this->findOrFail($id);
		return $oItem->delete();
	}

	public function addItem($arItem)
	{
		return $this->insert($arItem);
	}

	public function getDay()
	{
		return $this->whereDate('date_order', date('Y-m-d'))->sum('total_price');
		
	}

	public function getMonth()
	{
		return $this->whereMonth('date_order', date('m'))->sum('total_price');
		
	}

	public function getYear()
	{
		return $this->whereYear('date_order', date('Y'))->sum('total_price');	
	}
}
