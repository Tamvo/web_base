<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
  protected $table ='producttype';
  protected $primaryKey = 'id';
  public $timestamps=true;
  protected $fillable = ['id','name','id_cat','created_at','update_at'];

  public function products()
  {
    return $this->hasMany('App\Product', 'id_type');
  }

  public function getItems()
  {
    return $this->orderBy('id','DESC')->paginate(5);
  }

  public function getName($id){
    return $this->select('name')->where('id',$id)->latest('id')->first();
  }

  public function getAll()
  {
    return $this->orderBy('id','DESC')->get();
  }

  public function getItem($id)
  {
    return $this->findOrFail($id);
  }

  public function getId($id)
  {
    return $this->find($id);
  }

  public function getIdType($id)
  {
    return $this->where('id_cat',$id)->latest('id')->first();
  }

  public function getIdCat($id)
  {
    return $this->where('id_cat', $id)->get();
  }

  public function addItem($arItem)
  {
    return $this->insert($arItem);
  }

  public function editItems($id, $arItem)
  {
    $oItem = $this->findOrFail($id);
    return $oItem->update($arItem);
  }

  public function deleteItem($id)
  {
    $oItem = $this->findOrFail($id);
    return $oItem->delete();
  }

  public function deleteCat($id){
    $oItem = $this->where('id_cat', $id);
    return $oItem->delete();
  }
}
