<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Category;
use App\Contact;
use App\Product;
use App\ProductType;
use App\User;
use App\Customer;
use App\Bill;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        view::share('objContact', Contact::all());
        view::share('objQuanPro', Product::where('quantity',0)->get());
        view::share('objNewBill', Bill::where('active',0)->get());
        // view::share('objBill', Bill::orderBy('id','DESC')->where('active',0)->get());
        view::share('objProType', ProductType::all());
        view::share('objProduct', Product::all());
        view::share('objPro', Product::orderBy('price','ASC')->limit(5)->get());
        view::share('objUser', User::all());
        view::share('objCategory', Category::all());
        view::share('objCustomer', Customer::all());
        view::share('objBill', Bill::all());
       

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
