<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillDetail extends Model
{
  protected $table ='bill_detail';
  protected $primaryKey = 'id';
  public $timestamps=true;
  protected $fillable = ['id','id_bill', 'id_product','quantity','price', 'created_at','update_at'];

  public function bill()
  {
    return $this->belongsTo('App\Bill');
  }

  public function getItems($id)
  {
    return $this->join('product','product.id','=','bill_detail.id_product')
    ->select('bill_detail.*', 'product.name')
    ->where('id_bill','=',$id)
    ->get();
  }

  public function getItem($id)
  {
    return $this->join('bills','bills.id','=','bill_detail.id_bill')
    ->select('bills.*')
    ->where('bill_detail.id_bill','=',$id)
    ->get();
  }

  public function get($id)
  {
    return $this->findOrFail($id);
  }

  public function active($id)
  {
    $oItem =  $this->findOrFail($id);
    if( $oItem->active ==0){
      $oItem->active =1;
      return $oItem->save();
    }
  }

  public function deleteItem($id)
  {
    $oItem = $this->where('id_bill',$id)->get();
    return $oItem->delete();
  }

  public function addItem($arItem)
  {
    return $this->insert($arItem);
  }
}
