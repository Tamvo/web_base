<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CusDetail extends Model
{
  protected $table ='customer_detail';
  protected $primaryKey = 'id';
  public $timestamps=false;
  protected $fillable = ['id','id_user','address','phone','note'];

  public function getItems()
  {
    return $this->join('users','customer_detail.id_user','=','users.id')
    ->select('customer_detail.*', 'users.fullname')
    ->orderBy('id','DESC')
    ->paginate(5);
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function getIdUser($id)
  {
    return $this->where('id_user',$id)->latest('id')->first();
  }

  public function  deleteItem($id)
  {
    $oItem = $this->findOrFail($id);
    return $oItem->delete();
  }

  public function addItem($arItem)
  {
    return $this->insert($arItem);
  }
}
