<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $table ='category';
  protected $primaryKey = 'id';
  public $timestamps=false;
  protected $fillable = ['id','name'];

  public function getItems()
  {
    return $this->orderBy('id','DESC')->paginate(5);
  }

  public function getAll()
  {
    return $this->orderBy('id','DESC')->get();
  }

  public function getItem($id)
  {
    return $this->findOrFail($id);
  }

  public function getId($id){
    return $this->find($id);
  }

  public function getIdType($id)
  {
    return $this->where('id_cat', $id)->get();
  }

  public function addItem($arItem)
  {
    return $this->insert($arItem);
  }

  public function editItems($id, $arItem)
  {
    $oItem = $this->findOrFail($id);
    return $oItem->update($arItem);
  }

  public function deleteItem($id){
    $oItem = $this->findOrFail($id);
    return $oItem->delete();
  }

   //  public function active($id){
   //      $oItem =  $this->findOrFail($id);
   //      if( $oItem->active ==1){
   //          $oItem->active =0;
   //          return $oItem->save();
   //      }
   //      else{
   //        $oItem->active =1;
   //          return $oItem->save();
   //      }
   //  }
}
