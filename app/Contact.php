<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
  protected $table ='contact';
  protected $primaryKey = 'id';
  public $timestamps=true;
  protected $fillable = ['id','name','contact,active'];

  public function getItems()
  {
    return $this->orderBy('id','DESC')->paginate(5);
  }

  public function getItem($id)
  {
    return $this->findOrFail($id);
  }

  public function addItem($arItem)
  {
    return $this->insert($arItem);
  }
  
  public function deleteItem($id)
  {
    $oItem = $this->findOrFail($id);
    
    return $oItem->delete();
  }
}
