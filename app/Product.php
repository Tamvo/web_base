<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $table ='product';
  protected $primaryKey = 'id';
  public $timestamps=true;
  protected $fillable = ['id','name','quantity','price','sale', 'discription','id_type','picture','created_at','updated_at'];

  public function getItems()
  {
    return $this->orderBy('id','DESC')->paginate(9);
  }

  public function product_details()
  {
    return $this->hasMany('App\ProductDetail', 'id_product');
  }

  public function productType()
  {
    return $this->belongsTo('App\ProductType');
  }
  
  public function getItem($id)
  {
    return $this->findOrFail($id);
  }

  public function getId($id)
  {
    return $this->find($id);
  }

  public function getPro()
  {
    return $this->latest('id')->first();
  }

  public function addItem($arItem)
  {
    return $this->insert($arItem);
  }

  public function editItems($id, $arItem)
  {
    $oItem = $this->findOrFail($id);

    return $oItem->update($arItem);
  }

  public function deleteItem($id)
  {
    $oItem = $this->findOrFail($id);

    return $oItem->delete();
  }

  public function getIdType($id)
  {
    return $this->where('id_type',$id)->latest('id')->first();
  }

  public function getProType($id)
  {
    return $this->where('id_type',$id)->get();
  }

  public function getProType1($id)
  {
    return $this->where('id_type',$id)->paginate(9);
  }

  public function deleteProType($id)
  {
    $oItem = $this->where('id_type', $id);

    return $oItem->delete();
  }

  public function deleteCat($id)
  {
    $oItem = $this->where('id_cat', $id);

    return $oItem->delete();
  }

  public function getProduct()
  {
    return  $this->orderBy('id','DESC')->where('quantity','<>',0)->paginate(9);
  }

  public function search($search)
  {
    return $this->join('producttype', 'producttype.id','product.id_type')
    ->select('product.*')
    ->where('producttype.name','like','%'.$search.'%')
    ->orWhere('product.name','like','%'.$search.'%')
    ->orWhere('product.price','like','%'.$search.'%')
    ->orWhere('product.discription','like','%'.$search.'%')
    ->orWhere('product.sale','like','%'.$search.'%')
    ->paginate(6);
  }

  public function searchPro($search)
  {
    return $this->where('name','like','%'.$search.'%')->paginate(9);
  }

  public function searchPrice($search)
  {
    if($search > 10000000) {
      return $this->where('price','>=',$search)->paginate(9);
    }

    return $this->where('price','<=',$search)->paginate(9);
  }

  public function searchSale($search)
  {
    if($search > 60)
    {
      return $this->where('sale','>=', (int)$search)->paginate(9);
    }

    return $this->where('sale','<=', (int)$search)->paginate(9);
  }

  public function qtyactive($id, $sl)
  {
    $oItem =$this->findOrFail($id);
    $oItem->quantity = $sl;
    $oItem->save();
  }

  public  function upQty()
  {
    return $this->where('quantity',0)->get();
  }
}
