<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class MyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        // echo "Day la middleware";
        // die();
        // 
        $username = Auth::user()->role;
        if($username != 'admin' && $username != 'mod')
        {
            $username = 'customer';
        }
       
         $pos = strpos($role, $username, 0);
          // dd($pos);

         if(!empty($pos)){
         
            return $next($request);
        }
        else{
             // echo "Ban khong co quyen truy cap";
              return redirect()->route('loi');
        }
        // // 

    }
}
