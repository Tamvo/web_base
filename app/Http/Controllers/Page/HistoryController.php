<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Bill;
use App\BillDetail;

class HistoryController extends Controller
{
	public function __construct(Bill $modelBill, BillDetail $modelBillDetail)
    {
        $this->modelBill = $modelBill;
        $this->modelBillDetail = $modelBillDetail;
    }

    public function index(Request $request)
    {
        if(!Auth::check()) {
            $request->session()->flash('msg','Vui lòng đăng nhập');

            return redirect()->route('page.login.login');
        } else {
            $id_user = Auth::user()->id;
            $data = $this->modelBill->getBill($id_user);

            return view('page.index.history',compact('data'));
        }
    }
}
