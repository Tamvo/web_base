<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductType;

class ProductTypeController extends Controller
{
	public function __construct(Product $modelProduct,ProductType $modelProType)
	{
		$this->modelProduct =$modelProduct;
		$this->modelProType =$modelProType;
	}

    public function index($name, $id)
    {
    	$data = $this->modelProduct->getProType1($id);
    	$nameType =$this->modelProType->getName($id);
    	
    	return view('page.index.productType', compact('data', 'nameType'));
    }
}
