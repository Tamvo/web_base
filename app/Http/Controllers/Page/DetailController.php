<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class DetailController extends Controller
{
	public function __construct(Product $modelProduct)
	{
		$this->modelProduct = $modelProduct;
	}

    public function index($name, $id)
    {
    	$arr = $this->modelProduct->getItem($id);
        $id_type =  $arr->id_type;
        $proTypes = $this->modelProduct->getProType($id_type);

        return view('page.index.detail', compact('arr','proTypes'));
    }
}
