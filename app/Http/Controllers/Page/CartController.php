<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\CusDetail;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
	public function __construct(Product $modelProduct)
	{
		$this->modelProduct = $modelProduct;	
	}

	public function getcart()
	{
		$cartItems = Cart::content();

		return view('page.index.cart', compact('cartItems'));
	}

	public function addItem(Request $request)
	{
		$id = $request->id;
		$product = $this->modelProduct->getItem($id);

		if($product->sale === 0) {
			$price = round($product->price,-3);
		} else {
			$price = round(($product->price - ($product->price * $product->sale/100)),-3);
		}

		Cart::add($id,$product->name,1,$price,['picture' => $product->product_details->first()->picture, 'quantity' => $product->quantity]);
	}

	public function delete(Request $request)
	{
		$id = $request->rowId;
		Cart::remove($id);
		$cartItems =  Cart::content();

		return back();
	}
	
	public function update(Request $request)
	{
		$qty = $request->qty;
		$rowId = $request->rowId;

		Cart::update($rowId, $qty);

	}
}
