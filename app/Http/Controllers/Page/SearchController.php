<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ProductType;
use App\Product;

class SearchController extends Controller
{
    public function __construct(ProductType $modelProType, Product $modelProduct)
    {
    	$this->modelProType = $modelProType;
    	$this->modelProduct = $modelProduct;
    }

    public function getSearch(Request $request)
    {
    	$search = $request->search;
        $data =$this->modelProduct->search($search);

        return view('page.index.search',compact('data','search'));   
    }

    public function getSearchPro(Request $request)
    {
        $search = $request->search;
        $data =$this->modelProduct->searchPro($search);

        return view('page.index.search',compact('data','search'));
    }

    public function getSearchPrice(Request $request)
    {
        $search = $request->price;
        $data = $this->modelProduct->searchPrice($search);

        return view('page.index.searchPrice',compact('data','search'));
    }

    public function getSearchSale(Request $request){
        $search = $request->sale;
        $data = $this->modelProduct->searchSale($search);

        return view('page.index.searchPrice',compact('data','search'));
    }
}
