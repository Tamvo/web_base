<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\CusDetail;
Use App\Bill;
Use App\BillDetail;
Use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use DB;

class CheckoutController extends Controller
{
	public function __construct(Product $modelProduct)
	{
		
		$this->modelProduct = $modelProduct;
	}

	public function getToCheckout()
	{
		return view('page.index.tocheckout');
	}

	public function postToCheckout(Request $request)
	{
		$this->validate($request,[
			'fullname' => 'required',
			'address' =>'required',
			'phone' =>'required|regex:/(0)[0-9]{9}/',
		],[
			'fullname.required' => 'Vui lòng nhập họ tên',
			'address.required' => 'Vui lòng nhập địa chỉ nhận hàng',
			'phone.required' => 'Vui lòng nhập số điện thoại người nhận hàng',
			'phone.regex' => 'Số điện thoại không hợp lệ'

		]);
		$cartInfor = Cart::content();
		DB::beginTransaction();
		
		try {
            // save
			$cusDetail = new CusDetail;
			$cusDetail->address = $request->address;
			$cusDetail->id_user = Auth::user()->id;
			$cusDetail->note = $request->note;
			$cusDetail->phone = $request->phone;
			$cusDetail->save();

			$bill = new Bill;
			$bill->id_user = Auth::user()->id;
			$bill->date_order = date('Y-m-d H:i:s');
			$bill->total_price = str_replace(',', '', Cart::subtotal());
			$bill->active = 0;
			$bill->save();

			if (count($cartInfor) >0) {
				foreach ($cartInfor as $item) {
					$billDetail = new BillDetail;
					$billDetail->id_bill = $bill->id;
					$billDetail->id_product = $item->id;
					$billDetail->quantity = $item->qty;
					$billDetail->price = $item->price;
					$billDetail->save();
					$quantity = $item->options->quantity;
					$newQuantity = $quantity - $item->qty;
					$up = $this->modelProduct->qtyactive($item->id, $newQuantity);
				}
			}
          // del
			Cart::destroy();
			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			echo $e->getMessage();
		}

		return redirect()->route('page.index.thanks');
	}

	public function thanks()
	{
		return view('page.index.thanks');
	}
}
