<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Product;
use App\ProductType;
class IndexController extends Controller
{
	public function __construct(Product $modelProduct, ProductType $modelProType)
	{
		$this->modelProduct =$modelProduct;
		$this->modelProType =$modelProType;
	}

	public function index()
	{
		$data = $this->modelProduct->getProduct();
		
		return view('page.index.index', compact('data'));
	}
}
