<?php

namespace App\Http\Controllers\Page;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Cart;
class UserController extends Controller
{
	public function __construct(User $modelUser)
	{
		$this->modelUser = $modelUser;
	}

	public function getRegister()
	{
		return view('page.login.register');
	}

    public function postRegister(Request $request)
    {
        $this->validate($request,[
            'username' => 'required|unique:users',
            'fullname' => 'required',
            'email' => 'required',
            'password' => 'required',
        ],[
            'username.required' => 'Vui lòng nhập tên đăng nhập',
            'username.unique' => 'Tên đăng nhập đã tồn tại',
            'fullname.required' => 'Vui lòng nhập họ tên',
            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'email không đúng định dạng',
            'password.required' => 'Vui lòng nhập mật khẩu',
        ]);

        $oUser =  [
            'username' =>$request->username,
            'fullname' =>$request->fullname,
            'email' =>$request->email,
            'password' =>bcrypt($request->password),
            'role' => 'customer',
            'remember_token'=> $request->_token
        ];
        $oItem = $this->modelUser->addItem($oUser);

        if($oItem) {
            $request->session()->flash('msg','Đăng kí thành công, Vui lòng đăng nhập');
            return redirect()->route('page.login.login');
        } else {
            $request->session()->flash('error','Có lỗi');

            return redirect()->route('page.login.register');
        }
    }

    public function getLogin()
    {
        return view('page.login.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request,[
            'username' => 'required',
            'password' => 'required'
        ],[
            'username.required' => 'Vui lòng nhập tên đăng nhập',
            'password.required' => 'Vui lòng nhập mật khẩu'
        ]);
        $username = $request->username;
        $password = $request->password;
    	//cho phép so sánh nhìu giá trị
        $result = Auth::attempt([
            'username' => $username,
            'password' => $password
        ]);

        if($result) {
            return redirect()->route('page.index.index');
        } else {
            $request->session()->flash('error', 'Sai tên đăng nhập hoặc mật khẩu');

            return redirect()->route('page.login.login');
        }
    }

    public function logout()
    {
        Auth::logout();
        Cart::destroy();

        return redirect()->route('page.index.index');
    }
}
