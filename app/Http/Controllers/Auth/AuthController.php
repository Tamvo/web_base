<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class AuthController extends Controller
{
    public function getLogin()
    {
    	return view('auth.login');
    }

    public function postLogin(Request $request)
    {
    	$username = $request->username;
    	$password = $request->password;
    	//cho phép so sánh nhìu giá trị
    	$result = Auth::attempt([
    		'username' => $username,
    		'password' => $password
    	]);

    	if($result){
    		return redirect()->route('admin.index.index');
    	}
    	else{
    		$request->session()->flash('msg', 'Sai tên đăng nhập hoặc mật khẩu');
    		return redirect()->route('auth.login');
    	}
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('auth.login');
    }
}
