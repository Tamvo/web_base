<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bill;
use Auth;

class BillController extends Controller
{
	public function __construct(Bill $modelBill)
	{
		$this->modelBill = $modelBill;
	}

	public function index()
	{
		$oItems = $this->modelBill->getItems();

		return view('admin.bill.index', compact('oItems'));
	}

	public function del($id, Request $request)
	{
		$getId = $this->modelBill->getId($id);

		if(empty($getId))
		{
			$request->session()->flash('error','Đơn hàng không tồn tại');

			return redirect()->route('admin.bill.index');
		}
		$result1 = $this->modelBill->deleteItem($id);

		if ($result1)
		{
			$request->session()->flash('msg','Xóa thành công');
		} else {
			$request->session()->flash('msg','Có lỗi khi xóa');
		}

		return redirect()->route('admin.bill.index');
	}

	public function active(Request $request)
	{
		$id = $request->did;

		$oslide = $this->modelBill->getId($id);
		$kq = $this->modelBill->active($id);

		if ($kq) {
			if ($oslide->active == 0) {
				echo "Đã chuyển hàng";
			}
		} else {
			echo "alert('Lỗi')";
		}
	}

	public function billUser(){
		$id_user = Auth::user()->id;
		$oItems = $this->modelBill->getBill($id_user);

		return view('admin.bill.billuser', compact('oItems'));
	}
}
