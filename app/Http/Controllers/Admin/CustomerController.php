<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\CusDetail;

class CustomerController extends Controller
{
    public function __construct(Customer $modelCustomer, CusDetail $modelCusDetail){
    	$this->modelCustomer = $modelCustomer;
        $this->modelCusDetail = $modelCusDetail;
    }

    public function index(){
    	$oCustomers = $this->modelCustomer->getItems();

    	return view('admin.customer.index', compact('oCustomers'));
    }

    public function delete($id, Request $request)
    {
    	$oData = $this->modelCustomer->getId($id);

    	if(empty($oData)) {
    		$request->session()->flash('error','Khách hàng không tồn tại');

    		return redirect()->route('admin.customer.index');
    	}

        $oItem = $this->modelCusDetail->getIdUser($id);
        
        if(empty($oItem)) {
            $result = $this->modelCustomer->deleteItem($id);
            
            if($result) {
                $request->session()->flash('msg','Xóa thành công');
            } else {
                $request->session()->flash('msg','Có lỗi khi xóa');
            }

            return redirect()->route('admin.customer.index');
        } else{
            $data = $this->modelCusDetail->deleteItem($oItem->id);
            $query = $this->modelCustomer->deleteItem($id);

            if($query) {
                $request->session()->flash('msg','Xóa thành công');
            } else {
                $request->session()->flash('msg','Có lỗi khi xóa');
            }

            return redirect()->route('admin.customer.index');
        }
    }
}
