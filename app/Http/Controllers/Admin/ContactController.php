<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
   public function index()
   {
       return view('admin.contact.index');
   }

   public function show()
   {
       return view('admin.contact.show');
   }
   
   public function delete()
   {
       return view('admin.contact.delete');
   }
}
