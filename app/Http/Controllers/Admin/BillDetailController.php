<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CusDetail;
use App\BillDetail;
use App\User;

class BillDetailController extends Controller
{
    public function __construct(BillDetail $modelBilldetail, CusDetail $modelCusDetail, User $modelUser)
    {
        $this->modelBilldetail = $modelBilldetail;
        $this->modelCusDetail = $modelCusDetail;
        $this->modelUser =$modelUser;
    }

    public function index($id, Request $req)
    {
        $oItems = $this->modelBilldetail->getItems($id);

        return view('admin.billdetail.index', compact('oItems','id'));
    }

    public function del($id)
    {
        $result = $this->modelBilldetail->deleteItem($id);
        if ($result) {
            $request->session()->flash('msg','Xóa thành công');
        } else {
            $request->session()->flash('msg','Có lỗi khi xóa');
        }

        return redirect()->route('admin.bill.index');
    }

    public function create($id,Request $request)
    {
            //lấy thông tin đơn hàng
        $billDetail = $this->modelBilldetail->getItems($id);
           //lấy thông tin khách hàng
        $user = $this->modelUser->getUser($id);
        $id_user = $user->id_user;
        $cusDetail = $this->modelCusDetail->getIdUser($id_user);

        return view('admin.billdetail.creatHD', compact('billDetail','user','cusDetail'));
    }
}
