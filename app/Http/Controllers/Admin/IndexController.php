<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bill;

class IndexController extends Controller
{
	public function __construct(Bill $modelBill)
	{
		$this->modelBill = $modelBill;
	}

    public function index()
    {
    	$date_order = $this->modelBill->getDay();
    	$month_order = $this->modelBill->getMonth();
    	$year_order = $this->modelBill->getYear();
        
    	return view('admin.index.index', compact('date_order','month_order', 'year_order'));
    }
}
