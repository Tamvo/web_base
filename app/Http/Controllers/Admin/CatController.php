<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Category;
use App\ProductType;
use App\Product;
use App\ProductDetail;
class CatController extends Controller
{
    public function __construct(Category $modelCat, Product $modelProduct, ProductType $modelProType, ProductDetail $modelProDetail)
    {
        $this->modelCat = $modelCat;
        $this->modelProType = $modelProType;
        $this->modelProduct = $modelProduct;
        $this->modelProDetail = $modelProDetail;
    }

    public function index()
    {
        $data = $this->modelCat->getItems();

        return view('admin.category.index', compact('data'));
    }

    public function getAdd()
    {
    	return view('admin.category.add');
    }

    public function postAdd(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:category',
        ],[
            'name.required' => 'Vui lòng nhập tên danh mục',
            'name.unique' => 'Tên đã tồn tại',

        ]);

        $oCat =  ['name' =>$request->name];
        $oItem = $this->modelCat->addItem($oCat);

        if ($oItem)
        {
            $request->session()->flash('msg','Thêm thành công');
        } else {
            $request->session()->flash('msg','Có lỗi');
        }

        return redirect()->route('admin.category.index');
    }

    public function getUpdate($id, Request $request)
    {
         $oCat = $this->modelCat->getId($id);

        if (empty($oCat)) {
            $request->session()->flash('error','Danh mục không tồn tại');

            return redirect()->route('admin.category.index');   
        }

        return view('admin.category.update', compact('oCat'));
    }

    public function postUpdate($id, Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:category',
        ],[
            'name.required' => 'Vui lòng nhập tên danh mục',
            'name.unique' => 'Tên đã tồn tại',

        ]);
        $oCat =  $request->all();
        $oItem = $this->modelCat->editItems($id, $oCat);

        if ($oItem)
        {
            $request->session()->flash('msg','Cập nhật thành công');
        } else {
            $request->session()->flash('msg','Có lỗi');
        }

        return redirect()->route('admin.category.index');
    }

    public function delete($id, Request $request)
    {
        $getId = $this->modelCat->getId($id);

        if(empty($getId)) {
            $request->session()->flash('error','Danh mục không tồn tại');   
        } else {
            $dataType = $this->modelProType->getIdType($id);

            if(!empty($dataType)) {
                $dataPro = $this->modelProduct->getIdType($dataType->id);

                if(!empty($dataPro)) {  
                    $oProDetail= $this->modelProDetail->deletePro($dataPro->id);
                    $oProDetail= $this->modelProduct->deleteItem($dataPro->id);
                    $oType =  $this->modelProType->deleteItem($dataPro->id_type);
                    $oCat =  $this->modelCat->deleteItem($id);

                    if($oCat) {
                        $request->session()->flash('msg','Xóa thành công');
                    } else {
                        $request->session()->flash('msg','Có lỗi');
                    }  
                } else {
                    $oProType = $this->modelProType->deleteCat($id);

                    if($oProType) {
                        $oCat =  $this->modelCat->deleteItem($id);

                        if($oCat) {
                            $request->session()->flash('msg','Xóa thành công');
                        } else {
                            $request->session()->flash('msg','Có lỗi');
                        }
                    }
                }
            } else {
                $oType =  $this->modelCat->deleteItem($id);

                if($oType) {
                    $request->session()->flash('msg','Xóa thành công');
                } else {
                    $request->session()->flash('msg','Có lỗi');
                }
            }
        }

        return redirect()->route('admin.category.index');
    }
}
