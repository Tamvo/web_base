<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Admin\File;
use App\Product;
use App\ProductType;
use App\Category;
use App\ProductDetail;

class ProductController extends Controller
{
    public function __construct(Product $modelProduct, ProductType $modelProType, Category $modelCat, ProductDetail $modelProDetail)
    {
        $this->modelProduct = $modelProduct;
        $this->modelProType = $modelProType;
        $this->modelCat = $modelCat;
        $this->modelProDetail = $modelProDetail;

    }

    public function index()
    {
        $oProduct = $this->modelProduct->getItems();

        return view('admin.product.index', compact('oProduct'));
    }

    public function getAdd()
    {

        $oCats = $this->modelCat->getAll();

        return view('admin.product.add', compact('oCats'));
    }

    public function postAdd(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:product',
            'id_type' =>'required',
            'discription' =>'required',
            'price' =>'required',
            'sale' =>'required',
            'quantity' =>'required',
            'pictures' =>'required',

        ],[
            'name.required' => 'Vui lòng nhập tên sản phẩm',
            'name.unique' => 'Tên đã tồn tại',
            'id_type.required' => 'Vui lòng chọn loại sản phẩm',
            'discription.required' =>'Vui lòng thêm mô tả',
            'price.required' => 'Vui lòng thêm giá',
            'sale.required' =>'Vui lòng thêm giá khuyến mãi',
            'quantity.required' =>'Vui lòng nhập số sản phẩm hiện tại',
            'pictures.required' => 'Chọn ít nhất một ảnh'

        ]);
        $arProduct = [
            'name' => $request->name,
            'id_type' =>$request->id_type,
            'discription' =>$request->discription,
            'price' =>$request->price,
            'sale' =>$request->sale,
            'quantity' => $request->quantity,
        ];

        if($request->hasFile('pictures')) {
            $allowedfileExtension=['jpg','png','jpeg'];
            $files = $request->file('pictures');
            // flag xem có thực hiện lưu DB không. Mặc định là có
            $exe_flg = true;

            // kiểm tra tất cả các files xem có đuôi mở rộng đúng không
            foreach($files as $file) {
                $extension = $file->getClientOriginalExtension();
                
                $check=in_array($extension,$allowedfileExtension);

                if(!$check) {
                    $exe_flg = false;
                    return redirect()->back()->withErrors([ 'Không đúng định dạng file']);
                }
            }

            if($exe_flg) {
                // lưu product
                $products = $this->modelProduct->addItem($arProduct);

                if($products) {
                    $pro = $this->modelProduct->getPro($request->name);
                    
                    foreach ($request->file('pictures') as $picture) {
                        $image = time().$picture->getClientOriginalName();
                        $filename = $picture->move('files',$image );
                        $arProduct_detail= [
                            'id_product' => $pro->id,
                            'picture' => $image
                        ];
                        $product_detail = $this->modelProDetail->addItem($arProduct_detail);

                        if($product_detail) {
                            $request->session()->flash('msg','Thêm thành công');
                        } else {
                            $request->session()->flash('msg','Có lỗi khi thêm');
                        }
                    }
                }
            }
        }

        return redirect()->route('admin.product.index');
    }

    public function getUpdate($id, Request $request)
    {
        $getId = $this->modelProduct->getId($id);

        if(empty($getId)) {
            request->session()->flash('error','Sản phẩm không tồn tại');

            return redirect()->route('admin.product.index');
        }

        $oItem = $this->modelProduct->getItem($id);
        $oTypes =$this->modelProType->getAll();
        $id_type = $oItem->id_type;
        $oType = $this->modelProType->getItem($id_type);
        $id_cat = $oType->id_cat;
        $oCats =$this->modelCat->getAll();

        return view('admin.product.update', compact('oItem','oCats','oTypes','id_cat'));
    }

    public function postUpdate($id,Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'id_type' =>'required',
            'discription' =>'required',
            'price' =>'required',
            'sale' =>'required',
            'quantity' =>'required',

        ],[
            'name.required' => 'Vui lòng nhập tên sản phẩm',

            'id_type.required' => 'Vui lòng chọn loại sản phẩm',
            'discription.required' =>'Vui lòng thêm mô tả',
            'price.required' => 'Vui lòng thêm giá',
            'sale.required' =>'Vui lòng thêm giá khuyến mãi',
            'quantity.required' =>'Vui lòng nhập số sản phẩm hiện tại'

        ]);

        $oItem = $this->modelProDetail->getIdPros($id);
        $arProduct = [
            'name' => $request->name,
            'id_type' =>$request->id_type,
            'discription' =>$request->discription,
            'price' =>$request->price,
            'sale' =>$request->sale,
            'quantity' => $request->quantity,
        ];

        if($request->hasFile('pictures')) {   
            if($oItem != '')
            {
                foreach($oItem as $ima) {
                    unlink('files/'.$ima->picture );
                }
            }
            $allowedfileExtension=['jpg','png','jpeg'];
            $files = $request->file('pictures');
            // flag xem có thực hiện lưu DB không. Mặc định là có
            $exe_flg = true;

            // kiểm tra tất cả các files xem có đuôi mở rộng đúng không
            foreach($files as $file) {
                //tro den duoi file
                $extension = $file->getClientOriginalExtension();

                $check=in_array($extension,$allowedfileExtension);

                if(!$check) {
                    $exe_flg = false;
                    return redirect()->back()->withErrors([ 'Không đúng định dạng file']);
                }
            }

            if($exe_flg) {
                // lưu product$id
                $products = $this->modelProduct->editItems($id,$arProduct);

                if($products) {
                    $pro = $this->modelProDetail->getIdPro($id);
                    $id_pro =$pro->id_product;
                    $delete = $this->modelProDetail->deletePro($pro->id_product);

                    foreach ($request->file('pictures') as $picture) {

                        $image = time().$picture->getClientOriginalName();

                        $filename = $picture->move('files',$image );
                        $arProduct_detail= [
                            'id_product' => $id_pro,
                            'picture' => $image
                        ];
                        $product_detail = $this->modelProDetail->addItem($arProduct_detail);

                        if($product_detail) {
                            $request->session()->flash('msg','Cập nhật thành công');
                        } else {
                            $request->session()->flash('msg','Có lỗi khi cập nhật');
                        }
                    }
                }
            }
        } else {
            $products = $this->modelProduct->editItems($id,$arProduct);

            if($products) {
                $request->session()->flash('msg','Cập nhật thành công');
            } else {
                $request->session()->flash('msg','Có lỗi khi cập nhật');
            }
        }

        return redirect()->route('admin.product.index');
    }

    public function delete($id, Request $request)
    {
        $getId = $this->modelProduct->getId($id);
        
        if(empty($getId)) {
            $request->session()->flash('error','Sản phẩm không tồn tại');

            return redirect()->route('admin.product.index');
        }

        $oProDetail = $this->modelProDetail->getIdPro($id);

        if(empty($oProDetail)) {
            $oPro =  $this->modelProduct->deleteItem($id);
            if($oPro) {
                $request->session()->flash('msg','Xóa thành công');
            } else {
                $request->session()->flash('msg','Có lỗi');
            }
        } else {
            $oProDetail = $this->modelProDetail->deletePro($id);

            if($oProDetail) {
                $oPro =  $this->modelProduct->deleteItem($id);

                if($oPro) {
                    $request->session()->flash('msg','Xóa thành công');
                } else {
                    $request->session()->flash('msg','Có lỗi');
                }
            }

        }

        return redirect()->route('admin.product.index');
    }

    public function change(Request $request)
    {
        $id_cat = $request->id_cat;
        $oProType = $this->modelProType->getIdCat($id_cat);

        if($oProType) {
            foreach($oProType as $value) {
                echo "<option value='$value->id'>$value->name</option>";
            }
        } else {
            echo "<p class='alert alert-danger'>Đã xãy ra lỗi</p>";
        }
    }

    public function updateQty(){
        $oItems = $this->modelProduct->upQty();

        return view('admin.product.qty', compact('oItems'));
    }

    public function active(Request $request)
    {
        $sl =  $request->sl;
        $id_product =  $request->id_product;
        $data = $this->modelProduct->qtyactive($id_product,$sl);

    }
}
