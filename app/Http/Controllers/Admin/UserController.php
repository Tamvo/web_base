<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\User;
use App\CusDetail;
use Auth;

class UserController extends Controller
{
    public function __construct(User $modelUser, CusDetail $modelCusDetail){
        $this->modelUser = $modelUser;
        $this->modelCusDetail = $modelCusDetail;
    }

    public function index()
    {
        $oUsers = $this->modelUser->getItems();

        return view('admin.user.index',compact('oUsers'));
    }

    public function getAdd()
    {
    	return view('admin.user.add');
    }

    public function postAdd(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:users',
            'fullname' => 'required',
            'email' => 'required',
            'password' => 'required',
            'role' => 'required'
        ],[
            'username.required' => 'Vui lòng nhập tên đăng nhập',
            'username.unique' => 'Tên đăng nhập đã tồn tại',
            'fullname.required' => 'Vui lòng nhập họ tên',
            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'email không đúng định dạng',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'role.required' => 'Vui lòng chọn vai trò',
        ]);

        $oUser =  [
            'username' =>$request->username,
            'fullname' =>$request->fullname,
            'email' =>$request->email,
            'password' =>bcrypt($request->password),
            'role' => $request->role,
            'remember_token'=> $request->_token
        ];

        $oItem = $this->modelUser->addItem($oUser);

        if($oItem) {
            $request->session()->flash('msg','Thêm thành công');
        } else {
            $request->session()->flash('msg','Có lỗi');
        }

        return redirect()->route('admin.user.index');
    }

    public function getUpdate($id, Request $request)
    {
        $oUser = $this->modelUser->getId($id);
        
        if(empty($oUser))
        {
            $request->session()->flash('error','user không tồn tại');

            return redirect()->route('admin.user.index');
        }

        return view('admin.user.update', compact('oUser'));	
    }

    public function postUpdate($id, Request $request)
    {
        $this->validate($request,[
            'username' => 'required',
            'fullname' => 'required',
            'email' => 'required',
            'role' => 'required'
        ],[
            'username.required' => 'Vui lòng nhập tên đăng nhập',
            'username.unique' => 'Tên đăng nhập đã tồn tại',
            'fullname.required' => 'Vui lòng nhập họ tên',
            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'email không đúng định dạng',
            'password.required' => 'Vui lòng nhập mật khẩu',
            'role.required' => 'Vui lòng chọn vai trò',
        ]);
        $getId = $this->modelUser->getItem($id);
        $oUser =  [
            'username' =>$request->username,
            'fullname' =>$request->fullname,
            'email' =>$request->email,
            'password' =>$getId->password,
            'role' => $request->role,
            'remember_token'=> $request->_token
        ];

        if($request->password != '') {
            $oUser['password'] = bcrypt($request->password);
        }

        $result = $this->modelUser->editItems($id, $oUser);

        if($result) {
            $request->session()->flash('msg', 'Sửa thành công');
        } else {
            $request->session()->flash('msg', 'Có lỗi khi sửa');
        }

        return redirect()->route('admin.user.index');
    }

    public function delete($id, Request $request)
    {
        $getId = $this->modelUser->getId($id);

        if(empty($getId)) {
            $request->session()->flash('error','user không tồn tại');

            return redirect()->route('admin.user.index');
        } else {
            if($getId->role === 'admin') {
                $request->session()->flash('error','Không được xóa admin');

                return redirect()->route('admin.user.index');
            }
        }
        $data =  $this->modelUser->deleteItem($id);

        if($data) {
            $request->session()->flash('msg','Xóa thành công');
        } else {
            $request->session()->flash('msg','Có lỗi');
        }

        return redirect()->route('admin.user.index');
    }

    public function infor()
    {
        return view('admin.user.inforUserr');
    }

    public function addInfor(Request $request)
    {
        $this->validate($request,[
            'address' => 'required',
            'phone' => 'required',
        ],[
            'address.required' => 'Vui lòng nhập địa chỉ',
            'phone.required' => 'Vui lòng nhập số điện thoại',
        ]);

        $arInfor = [
            'id_user' => Auth::user()->id,
            'address' => $request->address,
            'phone' => $request->phone,
            'note' => $request->note
        ];

        $result = $this->modelCusDetail->addItem($arInfor);
        
        if($result) {
            $request->session()->flash('msg','Thêm thành công');
        } else {
            $request->session()->flash('msg','Có lỗi');
        }

        return back();
    }
}
