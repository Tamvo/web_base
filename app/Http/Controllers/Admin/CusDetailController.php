<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CusDetail;

class CusDetailController extends Controller
{
    public function __construct(CusDetail $modelCusDetail)
    {
    	$this->modelCusDetail = $modelCusDetail;
    }

    public function index()
    {
    	$oCustomers = $this->modelCusDetail->getItems();
        
    	return view('admin.customerdetail.index',compact('oCustomers'));
    }

    public function detail()
    {
    	return view('admin.customerdetail.detail');
    }
}
