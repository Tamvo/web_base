<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductType;
use App\Category;
use App\Product;
use App\ProductDetail;

class ProductTypeController extends Controller
{
	public function __construct(ProductType $modelProType,Category $modelCat, Product $modelProduct,ProductDetail $modelProDetail)
	{
		$this->modelProType = $modelProType;
		$this->modelCat = $modelCat;
        $this->modelProduct = $modelProduct;
        $this->modelProDetail = $modelProDetail;
    }

    public function index()
    {
    	$oItem = $this->modelProType->getItems();

    	return view('admin.productType.index', compact('oItem'));
    }

    public function getAdd()
    {
    	$oCats = $this->modelCat->all(); 

    	return view('admin.productType.add',compact('oCats'));
    }

    public function postAdd(Request $request)
    {
    	$this->validate($request,[
            'name' => 'required|unique:producttype',
        ],[
            'name.required' => 'Vui lòng nhập tên loại sản phẩm',
            'name.unique' => 'Tên đã tồn tại',

        ]);

        $oType =  [
        	'name' =>$request->name,
        	'id_cat' => $request->id_cat

        ];
        $oItem = $this->modelProType->addItem($oType);

        if($oItem) {
            $request->session()->flash('msg','Thêm thành công');
        } else {
            $request->session()->flash('msg','Có lỗi');
        }

        return redirect()->route('admin.productType.index');
    }

    public function getUpdate($id, Request $request)
    {
        $getId = $this->modelProType->getId($id);
        
        if(empty($getId)) {
            $request->session()->flash('error','Loại sản phẩm không tồn tại');

            return redirect()->route('admin.productType.index');     
        }

        $oCats = $this->modelCat->all();
        $oType = $this->modelProType->getItem($id);

        return view('admin.productType.update',compact('oType', 'oCats'));
    }

    public function postUpdate($id,Request $request)
    {
    	$this->validate($request,[
            'name' => 'required|unique:producttype',
        ],[
            'name.required' => 'Vui lòng nhập tên loại sản phẩm',
            'name.unique' => 'Tên đã tồn tại',

        ]);

        $oType =  [
        	'name' =>$request->name,
        	'id_cat' => $request->id_cat

        ];

        $oItem = $this->modelProType->editItems($id,$oType);

        if($oItem) {
            $request->session()->flash('msg','Sửa thành công');
        } else {
            $request->session()->flash('msg','Có lỗi');
        }

        return redirect()->route('admin.productType.index');
    }

    public function delete($id, Request $request)
    {
        $getId = $this->modelProType->getId($id);
        
        if(empty($getId)) {
            $request->session()->flash('error','Loại sản phẩm không tồn tại');

            return redirect()->route('admin.productType.index');
        }

        $is_null = $this->modelProduct->getIdType($id);

        if(!empty($is_null)) {
            $oProDetail= $this->modelProDetail->deletePro($is_null->id);
            $oPro = $this->modelProduct->deleteProType($id);

            if($oPro) {
                $oType =  $this->modelProType->deleteItem($id);

                if($oType) {
                    $request->session()->flash('msg','Xóa thành công');
                } else {
                    $request->session()->flash('msg','Có lỗi');
                }
            }
        } else {
            $oType =  $this->modelProType->deleteItem($id);

            if($oType) {
                $request->session()->flash('msg','Xóa thành công');
            } else {
                $request->session()->flash('msg','Có lỗi');
            }
        }
        
        return redirect()->route('admin.productType.index');
    }
}
