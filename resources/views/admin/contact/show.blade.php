@extends('templates.admin.master')
@section('title')
Trang liên hệ
@stop
@section('content')

<div class="form-title">
    <h4>Cập nhật người dùng</h4>
</div>
<form action="" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Họ tên</label>
            <input disabled="disable" type="text" class="form-control border-input" name="username" value="">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Email</label>
            <input type="text" disabled="disable" class="form-control border-input" name="email" value="">
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Liên hệ</label>
            <textarea disabled="disable" class="form-control border-input" name="preview"></textarea>
        </div>
    </div>
</div>
    <!-- <div class="text-center">
        <button type="submit" name="submit" class="btn btn-info btn-fill btn-wd">Cập nhật</button>
    </div> -->
    <div class="clearfix"></div>
</form>

@stop