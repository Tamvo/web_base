@extends('templates.admin.master')
@section('title')
Trang liên hệ
@stop
@section('content')
<div class="panel-body widget-shadow">
	
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Họ tên</th>
				<th>Email</th>
				<th>Liên hệ</th>
				
				<th>Chức năng</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">1</th>
				<td>Võ Xuân Xuân</td>
				<td>xuavo@gmail.com</td>
				<td>liên hệ làm gì đó</td>
				<td>
					<a href="{{ route('admin.contact.show')}}" class="btn btn-info"><i class="fa fa-pencil"></i>Xem</a>
					<a href="{{ route('admin.contact.delete')}}" class="btn btn-danger"><i class="fa fa-trash"></i>Xóa</a>
				</td>
				
			</tr>
			
		</tbody>
	</table>
</div>
@stop