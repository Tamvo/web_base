@extends('templates.admin.master')
@section('title')
Thêm người dùng
@stop
@section('content')

<div class="form-title">
    <h4>Thêm người dùng</h4>
</div>
<form action="{{ route('admin.user.add')}}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tên đăng nhập</label>
            <input type="text" class="form-control border-input" name="username">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control border-input" name="email">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Họ tên</label>
            <input type="text" class="form-control border-input" name="fullname">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Mật khẩu</label>
            <input type="text" class="form-control border-input" name="password">
        </div>
    </div>
</div>
<div class="text-center">
    <button type="submit" name="submit" class="btn btn-info btn-fill btn-wd">Thêm</button>
</div>
<div class="clearfix"></div>
</form>

@stop