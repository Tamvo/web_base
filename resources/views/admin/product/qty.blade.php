@extends('templates.admin.master')
@section('title')
Cập nhật số lượng sản phẩm
@stop
@section('content')
<div class="panel-body widget-shadow">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Danh sách bánh hết số lượng</h4>
                </div>
                @if(Session::has('msg'))
                <p class="alert alert-success">{{ Session::get('msg') }}</p>
                @endif
                <div class="content table-responsive table-full-width">
                    <table class="table">
                        <thead>
                            <th>ID</th>
                            <th>Tên bánh</th>
                            <th>Hình ảnh</th>
                            <th>Số lượng</th>
                        </thead>
                        <tbody>
                            @foreach($oItems as $oItem)
                            
                            <tr>
                               <td >{{$oItem->id}}</td>
                               <input type="hidden" name="id" class="idpro" value="{{$oItem->id}}">
                               <td>{{$oItem->name}}</td>
                               <td>
                                <img src="{{ asset('files/'.$oItem->product_details->first()->picture) }}" width="100px" height="80px">
                            </td>
                            <td><input type="number" id_product="{{$oItem->id}}" class="sl"  value="{{$oItem->quantity}}" min="0" ></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            
        </div>
    </div>


    <!--  -->
    
</div>
</div>
@stop
@section('js')

<script type="text/javascript">
 $(document).ready(function(){
    $('.sl').on('change', function(){
        let sl = $(this).val();
        let id_product = $(this).attr('id_product');
        $.ajax({
            type: 'GET',
            url : '{{ route('admin.product.active')}}',
            data: {sl: sl,id_product:id_product},
            success: function(data){
               window.location ='{{route('admin.product.qty')}}';
           }
       });
    });
});
 

</script>
@endsection
