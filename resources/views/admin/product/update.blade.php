@extends('templates.admin.master')
@section('title')
Cập nhật sản phẩm
@stop
@section('content')

<div class="form-title">
    <h4>Cập nhật sản phẩm</h4>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
</div>
<form action="" method="post" enctype="multipart/form-data" multiple="multiple">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tên sản phẩm</label>
            <input type="text" class="form-control border-input" value="{{ $oItem->name}}" name="name">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Danh mục sản phẩm</label><br/>
            <select name="id_cat" class="form-control border-input">
                <!-- <option value="">--Chọn--</option> -->
                @foreach($oCats as $oCat) 
                @if($id_cat === $oCat->id)
                <option selected="selected" value="{{ $oCat->id }}">{{ $oCat->name }}</option>
                @else
                <option value="{{ $oCat->id }}">{{ $oCat->name }}</option>
                @endif
                @endforeach
                <!-- <option value="">123</option> -->
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Loại sản phẩm sản phẩm</label><br/>
            <select name="id_type" class="form-control border-input">
                <!-- foeach  -->
                @foreach($oTypes as $oType) 
                @if($oItem->id_type === $oType->id)
                <option selected="selected" value="{{ $oType->id }}">{{ $oType->name }}</option>
                @else
                <option value="{{ $oType->id }}">{{ $oType->name }}</option>
                @endif
                @endforeach
                
                <!-- <option value="">123</option> -->
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Giá</label>
            <input type="text" class="form-control border-input" value="{{ $oItem->price}}" name="price" />
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Khuyến mãi</label>
            <input type="text" class="form-control border-input" value ="{{ $oItem->sale}}" name="sale" />
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>Số lượng</label>
            <input type="text" class="form-control border-input" value="{{ $oItem->quantity}}" name="quantity" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Mô tả</label>
            <textarea rows="4" class="form-control border-input" id="product-update" name="discription">{{ $oItem->discription}}</textarea>
        </div>
    </div>
</div>

<div class="row">
    @if($oItem->picture != '')
    <div class="col-md-4">
        <div class="form-group">
            
            <label>Ảnh cũ</label>
            <img src="../../../{{$oItem->picture}}" width="100" height="100">
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <input type="radio"  name="delete_picture">Xóa
        </div>
    </div>
    @endif
    <div class="col-md-4">
        <div class="form-group">
            <label>Chọn ảnh</label>
            <input type="file" name="pictures[]" multiple>
        </div>
    </div>
</div>


<div class="text-center">
    <button type="submit" name="submit" class="btn btn-info btn-fill btn-wd">Cập nhật</button>
</div>
<div class="clearfix"></div>
</form>

@stop
@section('js')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">

    CKEDITOR.replace( 'product-update' );
</script>
@stop