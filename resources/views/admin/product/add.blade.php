@extends('templates.admin.master')
@section('title')
Thêm sản phẩm
@stop
@section('content')

<div class="form-title">
    <h4>Thêm sản phẩm</h4>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(Session::has('msg'))
    <p class="alert alert-success">{{ Session::get('msg') }}</p>
    
    @endif
</div>
<form action="{{ route('admin.product.add')}}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tên sản phẩm</label>
            <input type="text" id="a" class="form-control border-input" value="" name="name">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Danh mục sản phẩm</label><br/>
            <select name="id_cat" id="id_cat" class="form-control border-input">
                <option value="">--Chọn--</option>}
                option
                @foreach($oCats as $oCat)
                <option value="{{ $oCat->id }}">{{ $oCat->name }}</option>
                @endforeach
                <!-- <option value="">123</option> -->
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Loại sản phẩm sản phẩm</label><br/>
            <select name="id_type" class="form-control border-input" id="id_type">

                <option value="">--Chọn--</option>

                <!-- <option value="">123</option> -->
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Giá</label>
            <input type="text" class="form-control border-input" name="price" />
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Khuyến mãi</label>
            <input type="text" class="form-control border-input" name="sale" />
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label>Số lượng</label>
            <input type="text" class="form-control border-input" name="quantity" />
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Mô tả</label>
            <textarea class="form-control border-input" name="discription" id="product-discription"></textarea>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Ảnh</label>
            <input type="file" class="form-control border-input" name="pictures[]" multiple>
        </div>
    </div>
</div>


<div class="text-center">
    <button type="submit" name="submit" class="btn btn-info btn-fill btn-wd">Thêm</button>
</div>
<div class="clearfix"></div>
</form>
@stop
@section('js')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">

    CKEDITOR.replace( 'product-discription' );

    $("#id_cat").change(function(){
        let id_cat = $('#id_cat').val();
        $.ajax({
            type:'GET',
            url: '{{route('admin.product.change')}}',
            data: {id_cat: id_cat},
            success: function(data){
                $('#id_type').html(data);
            }
        })
        // alert(id_cat);
    });
</script>
@stop


