@extends('templates.admin.master')
@section('title')
Trang sản phẩm
@stop
@section('content')
<div class="panel-body widget-shadow">
	<div class="header">
		<h4 class="title">Danh sách sản phẩm</h4>
		<!-- <p class="category">Here is a subtitle for this table</p> -->
	</div>
	<h4><a class="btn btn-primary" href="{{ route('admin.product.add')}}">Thêm</a></h4>
	@if(Session::has('msg'))
	<p class="alert alert-success">{{ Session::get('msg') }}</p>
	
	@endif
	@if(Session::has('error'))
	<p class="alert alert-danger">{{ Session::get('error') }}</p>
	
	@endif
	<table class="table">
		<thead>

			<tr>
				<th>ID</th>
				<th>Tên sản phẩm</th>
				<th>Số lượng</th>
				<th>Chức năng</th>
			</tr>
		</thead>
		<tbody>

			@foreach($oProduct as $oItem)
			@php
			
			$urlUpdate = route('admin.product.update',['id' => $oItem->id]);
			$urlDelete = route('admin.product.delete',['id' => $oItem->id]);
			@endphp
			<tr>
				<th scope="row">{{$oItem->id}}</th>
				<td>{{$oItem->name }}</td>
				<td>{{$oItem->quantity}}</td>
				<td>
					<a href="{{ $urlUpdate }}" class="btn btn-info"><i class="fa fa-pencil"></i>Sửa</a>
					<a href="{{ $urlDelete }}" class="btn btn-danger"><i class="fa fa-trash"></i>Xóa</a>
				</td>
				
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="row" style="color: red">{{ $oProduct->links() }}</div>
</div>
@stop