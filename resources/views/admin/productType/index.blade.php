@extends('templates.admin.master')
@section('title')
Trang loại sản phẩm
@stop
@section('content')
<div class="panel-body widget-shadow">
	<div class="header">
		<h4 class="title">Danh sách loại sản phẩm</h4>
		<!-- <p class="category">Here is a subtitle for this table</p> -->
	</div>
	<h4><a class="btn btn-primary" href="{{ route('admin.productType.add')}}">Thêm</a></h4>
	@if(Session::has('msg'))
	<p class="alert alert-success">{{ Session::get('msg') }}</p>
	
	@endif	
	@if(Session::has('error'))
	<p class="alert alert-danger">{{ Session::get('error') }}</p>
	
	@endif
	<table class="table">
		<thead>

			<tr>
				<th>ID</th>
				<th>Tên loại sản phẩm</th>
				
				<th>Chức năng</th>
			</tr>
			
		</thead>
		<tbody>
			@foreach($oItem as $oType)
			@php
			$urlUpdate = route('admin.productType.update',['id' => $oType->id]);
			$urlDelete = route('admin.productType.delete',['id' => $oType->id]);
			@endphp
			<tr>
				<th scope="row">{{$oType->id}}</th>
				<td>{{$oType->name}}</td>
				<td>
					<a href="{{ $urlUpdate }}" class="btn btn-info"><i class="fa fa-pencil"></i>Sửa</a>
					<a href="{{ $urlDelete }}" class="btn btn-danger"><i class="fa fa-trash"></i>Xóa</a>
				</td>
				
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="row" style="color: red">{{ $oItem->links() }}</div>
</div>
@stop