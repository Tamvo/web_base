@extends('templates.admin.master')
@section('title')
Cập nhật loại sản phẩm
@stop
@section('content')

<div class="form-title">
    <h4>Cập nhật loại sản phẩm</h4>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>

<form action="{{ route('admin.productType.update',['id' => $oType->id])}}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tên loại sản phẩm</label>
            <input type="text" class="form-control border-input" value="{{ $oType->name }}" name="name">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Danh mục sản phẩm</label><br/>
            <select name="id_cat" class="form-control border-input">

                @foreach($oCats as $oCat) 
                @if($oType->id_cat === $oCat->id)
                <option selected="selected" value="{{ $oCat->id }}">{{ $oCat->name }}</option>
                @else
                <option value="{{ $oCat->id }}">{{ $oCat->name }}</option>
                @endif
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="text-center">
    <button type="submit" name="submit" class="btn btn-info btn-fill btn-wd">Cập nhật</button>
</div>
<div class="clearfix"></div>
</form>

@stop