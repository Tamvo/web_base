@extends('templates.admin.master')
@section('title')
Thêm loại sản phẩm
@stop
@section('content')

<div class="form-title">
    <h4>Thêm danh mục</h4>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<form action="{{ route('admin.productType.add')}}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tên loại sản phẩm</label>
            <input type="text" class="form-control border-input" name="name">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Danh mục sản phẩm</label><br/>
            <select name="id_cat" class="form-control border-input">
                @foreach($oCats as $oCat) 
                <option value="{{ $oCat->id }}">{{ $oCat->name }}</option>
                @endforeach
                <!-- <option value="">123</option> -->
            </select>
        </div>
    </div>
</div>
<div class="text-center">
    <button type="submit" name="submit" class="btn btn-info btn-fill btn-wd">Thêm</button>
</div>
<div class="clearfix"></div>
</form>

@stop