@extends('templates.admin.master')
@section('content')
<div class="panel-body widget-shadow">
    <div class="header">
        <h4 class="title">Danh sách đơn hàng</h4>
        <!-- <p class="category">Here is a subtitle for this table</p> -->
    </div>
    @if(Session::has('msg'))
    <p class="alert alert-success">{{ Session::get('msg') }}</p>
    @endif
    
    @if(Session::has('error'))
    <p class="alert alert-danger">{{ Session::get('error') }}</p>
    
    @endif
    <table class="table">
        <thead  style="text-align: center;">
            <th>ID</th>
            <th>Tên KH</th>
            <th>Ngày đặt</th>
            <th>Tổng tiền</th>
            <th>Trạng thái</th>
            <th>Chức năng</th>
        </thead>
        <tbody>
            @foreach($oItems as $oItem)
            @php
            $urlDetail = route('admin.customerdetail.index',['id'=>$oItem->id_user]);
            @endphp

            <tr>
               <td>{{$oItem->id}}</td>
               <td>
                <a href="{{ $urlDetail }}"></i>{{$oItem->fullname}}</a>
            </td>
            <td>{{$oItem->date_order}}</td>
            <td>{{number_format($oItem->total_price,0,",",".") }}đ</td>
            <td id="active{{$oItem->id}}">
                @if($oItem->active == 1)
                <p  >Đã chuyển hàng<p>
                    @else
                    <a href="javascript:void(0)" class="doi btn btn-danger" doi="{{$oItem->id}}" >Xác nhận chuyển hàng</a>

                    @endif
                </td>
                <td>
                    <a href="{{route('admin.billdetail.index',$oItem->id)}}" class="seen" xem="{{$oItem->id}}"  >
                        <i class="ti-eye btn btn-success">Xem</i>
                    </a> &nbsp;||&nbsp;
                    <a href="{{route('admin.bill.del',$oItem->id)}}" ><i class="ti-trash btn btn-danger">Xóa</i> </a>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
    <div class="row" style="color: red">{{ $oItems->links() }}</div>
</div>
@stop
@section('js')
<script type="text/javascript">
    $('.doi').on('click', function(){
        let id = $(this).attr('doi');
        let count = {{count($objNewBill)}};

        
        $.ajax({
            url: '{{route('admin.bill.active')}}',
            type: 'GET',
            data: {did:id},
            success: function(data){
             
                $('#active'+id).html(data);
            },
            error: function (){
              alert('Có lỗi xảy ra');
          }
      });
    });

    // });
</script>
@endsection
