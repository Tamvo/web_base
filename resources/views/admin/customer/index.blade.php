@extends('templates.admin.master')
@section('title')
Trang khách hàng
@stop
@section('content')
<div class="panel-body widget-shadow">
	<div class="header">
		<h4 class="title">Danh sách khách hàng</h4>
		<!-- <p class="category">Here is a subtitle for this table</p> -->
	</div>
	@if(Session::has('msg'))
	<p class="alert alert-success">{{ Session::get('msg') }}</p>
	
	@endif
	@if(Session::has('error'))
	<p class="alert alert-danger">{{ Session::get('error') }}</p>
	
	@endif
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Tên đăng nhập</th>
				<th>Họ tên</th>
				<th>Xem chi tiết</th>
				<th>Chức năng</th>
			</tr>
		</thead>
		<tbody>
			@foreach($oCustomers as $oCustomer )
			@php
			
			$urlDelete = route('admin.customer.delete',['id' =>$oCustomer->id]);
			$urlDetail = route('admin.customerdetail.index',['id'=>$oCustomer->id]);
			@endphp
			<tr>
				<th scope="row">{{ $oCustomer->id}}</th>
				<td>{{ $oCustomer->username }}</td>
				<td>{{ $oCustomer->fullname }}</td>
				<td><a href="{{ $urlDetail }}" class="btn btn-info"><i class="fa fa-trash"></i>Xem chi tiết</a></td>

				<td>
					<a href="{{ $urlDelete }}" class="btn btn-danger"><i class="fa fa-trash"></i>Xóa</a>
				</td>
				
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="row" style="color: red">{{ $oCustomers->links() }}</div>
</div>
@stop