
@extends('templates.admin.master')
@section('title')
Trang chủ
@stop
@section('content')
<div class="panel-body widget-shadow">
	<h4>Thông kế</h4>
	<div class="item">
		Thu nhập hôm nay
		<span class="item-price">{{ number_format(round($date_order,-3),0,",",".") }}đ</span>
	</div>
	<div class="item">
		Thu nhập trong tháng
		<span class="item-price">{{ number_format(round($month_order,-3),0,",",".") }}đ</span>
	</div>
	<div class="item">
		Thu nhập trong năm
		<span class="item-price">{{ number_format(round($year_order,-3),0,",",".") }}đ</span>
	</div>
	
</div>
@stop