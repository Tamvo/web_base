@extends('templates.admin.master')
@section('content')

<div class="panel-body widget-shadow creat">
    <span><a href="{{ route('admin.billdetail.creatHD',['id' => $id]) }}"  id="creatHD" id_bill="{{$id}}">Tạo hóa đơn</a></span>
    <div class="header">
        <h4 class="title">Chi tiết đơn hàng</h4>
        <!-- <p class="category">Here is a subtitle for this table</p> -->
    </div>
    
    <table class="table">
        <thead  style="text-align: center;">
            <th>ID</th>
            <th>Sản Phẩm</th>
            <th>Số lượng</th>
            <th>Đơn giá</th>
            <th>Tổng tiền</th>
        </thead>
        <tbody>
            @php $count = count($oItems); @endphp
            @foreach($oItems as $oItem)
            <tr>
                <td>{{$oItem->id}}</td>
                
                <td>{{$oItem->name}}</td>
                <td>{{$oItem->quantity}}</td>
                <td>{{number_format($oItem->price,0,",",".")}}đ</td>
                <!-- <td>{{$oItem->id}}</td> -->
                <td>
                 {{number_format($oItem->quantity * $oItem->price,0,",",".")}}đ
             </td>
         </tr>
         @endforeach
              <!-- <tr>
                  <td colspan="4">Tổng tiền</td>
                  <td colspan="4">{{ Cart::subtotal() }}đ</td>
              </tr> -->
              <tr>
                <td colspan="5"></td>
                <td ></td>
            </tr>
        </tbody>
    </table>

</div>

@stop
