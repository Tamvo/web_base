
@extends('templates.admin.master')
@section('title')
Tạo hóa đơn
@stop
@section('content')
<div class="panel-body widget-shadow creat">
    <div class="header">
        <a href="" onclick="window.print();"><i class="fa fa-print" aria-hidden="true"></i>In hóa đơn</a>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h4 class="title">Hóa đơn bán lẻ</h4>
            </div>
            <div class="content table-responsive table-full-width">
             <table class="table" id="tabledata">
                <div class="row">
                    <div class="col-md-7" style="">
                        <span style="color: red; font-size: 18px; line-height: 30px;">Shoppy</span>
                        <span>Địa chỉ: 81 Quang Trung Đà Nẵng</span>
                        <span>Số ĐT: 0123456789 </span>
                        <span>Ngày: {{ date('d-m-Y H:i')}}</span>
                        <span>Nhân viên: {{ Auth::user()->fullname }}</span>

                    </div>
                    <div class="col-md-3">
                        <span style="color: red; font-size: 18px; line-height: 30px;">Khách hàng</span>
                        <span>MaHD: {{ $user->id }}</span>
                        <span>Tên KH: {{ $user->fullname }}</span>
                        <span>Địa Chỉ: {{ $cusDetail->address }}</span>
                        <span>SDT: {{ $cusDetail->phone }}</span>
                    </div>
                </div>
            </table>
            <div class="content table-responsive ">
                <table class="table table-bordered">
                    <thead>
                        <td>Sản Phẩm </td>
                        <td>Số lượng</td>
                        <td>Đơn giá</td>
                        <td>Thành tiền</td>
                    </thead>
                    <tbody>
                        @foreach($billDetail as $bill)
                        <tr>
                            <td>
                                {{ $bill->name }}
                            </td>
                            <td>
                                {{ $bill->quantity }}
                            </td>
                            <td>
                                {{ number_format($bill->price,0,",",".") }}$
                            </td>
                            <td>
                                {{ number_format($bill->quantity * $bill->price,0,",",".") }}$
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="3" class="text-right"><b>Tổng tiền</b></td>
                            <td><b>{{number_format($user->total_price,0,",",".")}}đ</b></td>
                        </tr>
                    </tbody>
                </table>
                <div style="height: 300px;" class="row">
                    <div class="col-md-10" style=" padding-left: 20px;">Khách hàng kí xác nhận</div>
                    <div class="col-md-2" style="float: right;">
                     <br />
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>
</div>




<style type="text/css">
span{
  display: block;
  font-weight: 500;
}
</style>
@stop