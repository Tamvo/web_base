@extends('templates.admin.master')
@section('title')
Trang người dùng
@stop
@section('content')
<div class="panel-body widget-shadow">
	<div class="header">
		<h4 class="title">Danh sách người dùng</h4>
		<!-- <p class="category">Here is a subtitle for this table</p> -->
	</div>
	<h4><a class="btn btn-primary" href="{{ route('admin.user.add')}}">Thêm</a></h4>
	@if(Session::has('msg'))
	<p class="alert alert-success">{{ Session::get('msg') }}</p>
	
	@endif
	@if(Session::has('error'))
	<p class="alert alert-danger">{{ Session::get('error') }}</p>
	
	@endif
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Tên đăng nhập</th>
				<th>Email</th>
				<th>Chức vụ</th>
				<th>Chức năng</th>
			</tr>
		</thead>
		<tbody>
			@foreach($oUsers as $oUser )
			@php
			$urlUpdate = route('admin.user.update',['id' =>$oUser->id]);
			$urlDelete = route('admin.user.delete',['id' =>$oUser->id]);
			@endphp
			<tr>
				<th scope="row">{{ $oUser->id}}</th>
				<td>{{ $oUser->username }}</td>
				<td>{{ $oUser->email }}</td>
				<td>{{ $oUser->role }}</td>

				<td>
					<a href="{{ $urlUpdate }}" class="btn btn-info"><i class="fa fa-pencil"></i>Sửa</a>
					<a href="{{ $urlDelete }}" class="btn btn-danger"><i class="fa fa-trash"></i>Xóa</a>
				</td>
				
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="row" style="color: red">{{ $oUsers->links() }}</div>
</div>
@stop