@extends('templates.admin.master')
@section('title')
Cập nhật người dùng
@stop
@section('content')

<div class="form-title">
    <h4>Cập nhật người dùng</h4>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<form action="" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tên đăng nhập</label>
            <input type="text" class="form-control border-input" name="username" value="{{ $oUser->username }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control border-input" name="email" value="{{ $oUser->email }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Họ tên</label>
            <input type="text" class="form-control border-input" name="fullname" value="{{ $oUser->fullname }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Mật khẩu</label>
            <input type="password" class="form-control border-input" name="password">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Chức vụ</label><br>
            <select name="role" class="form-control border-input">
                @if($oUser->role === 'admin' )
                <option value="">--Chọn--</option>
                <option value="admin" selected="selected">Admin</option>
                <option value="mod">mod</option>
                <option value="user">user</option>
                @elseif($oUser->role === 'mod')
                <option value="">--Chọn--</option>
                <option value="admin" >Admin</option>
                <option value="mod" selected="selected">mod</option>
                <option value="user">user</option>
                @else
                <option value="">--Chọn--</option>
                <option value="admin" >Admin</option>
                <option value="mod">mod</option>
                <option value="user" selected="selected">user</option>
                @endif
            </select>
        </div>
    </div>
</div>
<div class="text-center">
    <button type="submit" name="submit" class="btn btn-info btn-fill btn-wd">Cập nhật</button>
</div>
<div class="clearfix"></div>
</form>

@stop