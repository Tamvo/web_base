

@extends('templates.admin.master')
@section('content')
<div class="panel-body widget-shadow">
	<h3>
		Trang cá nhân
	</h3>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<h5>Tên đăng nhập: <i>{{ Auth::user()->username }}</i></h5>
				<h5>Họ tên: <i>{{ Auth::user()->fullname }}</i></h5>
				<h5>Email: <i>{{ Auth::user()->email }}</i></h5>
				
			</div>
			<div class="col-md-6">
				@if(empty(Auth::user()->cusDetail))
				<h4><a href="javascript:void(0)" class="addInfor">Thêm thông tin</a></h4>
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
				<div class="information">
					<form action="{{ route('admin.user.inforUser') }}" method="post" >
						{{ csrf_field() }}
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label>Địa Chỉ</label>
									<input type="text" class="form-control border-input" name="address">
								</div>
							</div>
						</div>
						<div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Số Điện thoại</label>
									<input type="text" class="form-control border-input" name="phone">
								</div>
							</div>
						</div>
						<div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Ghi Chú</label>
									<textarea class="form-control border-input" name="note"></textarea>
								</div>
							</div>
						</div>
						<div>
							<div class="col-md-12">
								<div class="form-group">
									<input type="submit" class="btn btn-warning" name="submit" value="Thêm">
								</div>
							</div>
						</div>
					</form>
				</div>
				@else
				<h5>Địa Chỉ: <i>{{ Auth::user()->username }}</i></h5>
				<h5>Số Điện thoại: <i>{{ Auth::user()->fullname }}</i></h5>
				<h5>Ghi Chú: <i>{{ Auth::user()->email }}</i></h5>
				<h4><a href="javascript:void(0)">Sửa thông tin</a></h4>
				@endif
			</div>
		</div>
	</div>
	
</div>
@stop
@section('js')
<script type="text/javascript">
	$(document).ready(function(){
		$('.information').hide();
		$('.addInfor').on('click', function(){
			$('.information').show();
		});
	});
</script>
@stop