@extends('templates.admin.master')
@section('title')
Trang khách hàng
@stop
@section('content')
<div class="panel-body widget-shadow">
	
	@if(Session::has('msg'))
	<p class="alert alert-success">{{ Session::get('msg') }}</p>
	
	@endif
	@if(Session::has('error'))
	<p class="alert alert-danger">{{ Session::get('error') }}</p>
	
	@endif
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Họ tên</th>
				<th>Địa chỉ</th>
				<th>Số điện thoại</th>
				
			</tr>
		</thead>
		<tbody>
			@foreach($oCustomers as $oCustomer )
			
			<tr>
				<th scope="row">{{ $oCustomer->id}}</th>
				<td>{{ $oCustomer->fullname }}</td>
				<td>{{ $oCustomer->address }}</td>
				<td>{{ $oCustomer->phone }}</td>

				
				
			</tr>
			@endforeach
		</tbody>
	</table>

</div>
@stop