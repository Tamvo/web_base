@extends('templates.admin.master')
@section('title')
Sửa danh mục
@stop
@section('content')

<div class="form-title">
    <h4>Sửa danh mục</h4>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<form action="{{route('admin.category.update',['id' => $oCat->id])}}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label>Tên danh mục</label>
            <input type="text" class="form-control border-input" name="name" value="{{ $oCat->name }}">
        </div>
    </div>
</div>
<div class="text-center">
    <button type="submit" name="submit" class="btn btn-info btn-fill btn-wd">Cập nhật</button>
</div>
<div class="clearfix"></div>
</form>

@stop