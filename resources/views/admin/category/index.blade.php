@extends('templates.admin.master')
@section('title')
Trang danh mục
@stop
@section('content')
<div class="panel-body widget-shadow">
	<div class="header">
		<h4 class="title">Danh sách danh mục sản phẩm</h4>
		<!-- <p class="category">Here is a subtitle for this table</p> -->
	</div>
	<h4><a class="btn btn-primary" href="{{ route('admin.category.add')}}">Thêm</a></h4>
	@if(Session::has('msg'))
	<p class="alert alert-success">{{ Session::get('msg') }}</p>
	
	@endif	
	@if(Session::has('error'))
	<p class="alert alert-danger">{{ Session::get('error') }}</p>
	
	@endif	
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Tên danh mục</th>
				
				<th>Chức năng</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $Item)
			@php
			$urlUpdate = route('admin.category.update',['id' => $Item->id]);
			$urlDelete = route('admin.category.delete',['id' => $Item->id]);
			@endphp
			<tr>
				<th scope="row">{{$Item->id}}</th>
				<td>{{$Item->name}}</td>
				<td>
					<a href="{{ $urlUpdate }}" class="btn btn-info"><i class="fa fa-pencil"></i>Sửa</a>
					<a href="{{ $urlDelete }}" class="btn btn-danger"><i class="fa fa-trash"></i>Xóa</a>
				</td>
				
			</tr>
			@endforeach
		</tbody>
		
		
	</table>
	<div class="row" style="color: red">{{ $data->links() }}</div>
</div>
@stop