<!--
    Author: W3layouts
    Author URL: http://w3layouts.com
    License: Creative Commons Attribution 3.0 Unported
    License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta tags -->
    <title>Đăng nhập</title>
    <meta name="keywords" content="Winter Login Form Responsive widget, Flat Web Templates, Android Compatible web template, 
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- stylesheets -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/style1.css')}}">
    <!-- google fonts  -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet">
</head>
<body>
    <div class="agile-login">
        <h1>Mời đăng nhập</h1>
        <div class="wrapper">
            <h2>Đăng nhập</h2>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li style="color: #fff;">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if(Session::has('msg'))
            <p style=" color: #fff;">{{ Session::get('msg') }}</p>
            @endif
            <div class="w3ls-form">
                <form action="{{ route('auth.login') }}" method="post">
                    {{ csrf_field() }}
                    <label>Username</label>
                    <input type="text" name="username" placeholder="Username" />
                    <label>Password</label>
                    <input type="password" name="password" placeholder="Password"  />
                    <!-- <a href="#" class="pass">Forgot Password ?</a> -->
                    <input type="submit" value="Đăng nhập" />
                </form>
            </div>
            <div class="copyright">
                <p>© 2018 <a href="#" target="_blank">Võ Thị Xuân.</a></p>
            </div>
        </div>
        
    </body>
    </html>