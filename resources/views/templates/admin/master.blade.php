@include('templates.admin.header')

<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		
		@yield('content')  
		
	</div>
</div>
@yield('js')
@include('templates.admin.footer')