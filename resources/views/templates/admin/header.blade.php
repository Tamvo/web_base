

<!DOCTYPE HTML>
<html>
<head>
	<title class="title-a">Admin - @yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- Bootstrap Core CSS -->
	<link href="{{ asset('css/bootstrap.css') }}" rel='stylesheet' type='text/css' />
	<!-- Custom CSS -->
	<link href="{{ asset('css/style.css') }}" rel='stylesheet' type='text/css' />
	<!-- font CSS -->
	<!-- font-awesome icons -->
	<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet"> 
	<!-- //font-awesome icons -->
	<!-- js-->
	<script src="{{ asset('js/jquery-1.11.1.min.js')}}"></script>
	<script src="{{ asset('js/modernizr.custom.js')}}"></script>
	<!--webfonts-->
	<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
	<!--//webfonts--> 
	<!--animate-->
	<link href="{{ asset('css/animate.css')}}" rel="stylesheet" type="text/css" media="all">
	<script src="{{ asset('js/wow.min.js')}}"></script>
	<script>
		new WOW().init();
	</script>
	<!--//end-animate-->
	<!-- Metis Menu -->
	<script src="{{ asset('js/metisMenu.min.js')}}"></script>
	<script src="{{ asset('js/custom.js')}}"></script>
	<link href="{{ asset('css/custom.css')}}" rel="stylesheet">
	<!--//Metis Menu -->
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
		<div class=" sidebar" role="navigation">
			<div class="navbar-collapse">
				<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
					<ul class="nav" id="side-menu">
						@if(Auth::user()->role === 'admin' || Auth::user()->role === 'mod' )
						<li>
							<a href="{{ route('admin.index.index')}}"><i class="fa fa-home nav_icon"></i>Trang chủ</a>
						</li>
						<li>
							<a href="{{ route('admin.bill.index')}}"><i class="fa fa-th-large nav_icon"></i>Danh sách đơn hàng<span class="nav-badge-btm">{{count($objBill)}}</span></a>
						</li>
						
						<li>

							<a href="{{ route('admin.category.index')}}"><i class="fa fa-th-large nav_icon"></i>Danh mục sản phẩm <span class="nav-badge-btm">{{count($objCategory)}}</span></a>
						</li>
						<li>
							<a href="{{ route('admin.productType.index')}}"><i class="fa fa-th-large nav_icon"></i>Loại sản phẩm <span class="nav-badge-btm">{{count($objProType)}}</span></a>
						</li>
						<li class="">
							<a href="{{ route('admin.product.index')}}"><i class="fa fa-book nav_icon"></i>Sản Phẩm<span class="nav-badge-btm">{{count($objProduct)}}</span></a>
						</li>
						<li>
							<a href="{{ route('admin.user.index')}}"><i class="fa fa-envelope nav_icon"></i>Người dùng<span class="nav-badge-btm">{{count($objUser)}}</span></a>

							
						</li>
						@else
						<li>
							<a href="{{ route('admin.bill.billuser') }}"><i class="fa fa-th-large nav_icon"></i>Đơn hàng của bạn<span class="nav-badge-btm">{{count( DB::table('bills')->where('id_user', Auth::user()->id)->get())}}</span></a>
						</li>
						@endif
						<!-- <li>
							<a href="{{ route('admin.customer.index')}}"><i class="fa fa-envelope nav_icon"></i>Khách hàng<span class="nav-badge-btm">{{count($objCustomer)}}</span></a>
							
						</li> -->
					<!-- 	<li>
							<a href="{{ route('admin.contact.index')}}"><i class="fa fa-table nav_icon"></i>Liên hệ <span class="nav-badge-btm">{{count($objContact)}}</span></a>
						</li>
						<li>
							<a href="#"><i class="fa fa-check-square-o nav_icon"></i>Slide<span class="nav-badge-btm">07</span></a>
							
							
						</li> -->
						
					</ul>
					<div class="clearfix"> </div>
					<!-- //sidebar-collapse -->
				</nav>
			</div>
		</div>
		<!--left-fixed -navigation-->
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
				<!--toggle button start-->
				<button id="showLeftPush"><i class="fa fa-bars"></i></button>
				<!--toggle button end-->
				<!--logo -->
				<div class="logo">
					<a href="{{ route('admin.index.index')}}">
						<h1>ADMIN</h1>
						<span>AdminPanel</span>
					</a>
				</div>
				<!--//logo-->
				
				<div class="clearfix"> </div>
			</div>
			<div class="header-right">
				<div class="profile_details_left"><!--notifications of menu start -->
					<ul class="nofitications-dropdown">

						<li class="dropdown head-dpdn">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue count-bill">{{count($objNewBill)}}</span></a>
							<ul class="dropdown-menu">
								<li>
									<div class="notification_header">
										<h3>Bạn có {{count($objNewBill)}} đơn hàng mới</h3>
									</div>
								</li>
								@foreach($objNewBill as $data)
								<li><a href="{{ route('admin.bill.index')}}">
									
									<div class="notification_desc">
										<p>Tổng tiền {{ number_format($data->total_price,0,",",".")    }}đ</p>
										
									</div>
									<div class="clearfix"></div>	
								</a></li>
								@endforeach
								@if(count($objNewBill) != 0)
								<li>
									<div class="notification_bottom">
										<a href="{{ route('admin.bill.index')}}">Xem tất cả</a>
									</div> 
								</li>
								@endif
							</ul>
						</li>	
						<li class="dropdown head-dpdn">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">{{  count($objQuanPro) }}</span></a>
							<ul class="dropdown-menu">
								<li>
									<div class="notification_header">
										<h3>Bạn có {{  count($objQuanPro) }} sản phẩm đã hết hàng</h3>
									</div>
								</li>
								@foreach($objQuanPro as $value)
								<li><a href="#">
									
									<div class="notification_desc">
										<p>{{ $value->name }}</p>
										
									</div>
									<div class="clearfix"></div>	
								</a></li>
								@endforeach
								@if(count($objQuanPro) != 0)
								<li>
									<div class="notification_bottom">
										<a href="{{ Route('admin.product.qty') }}">Xem tất cả</a>
									</div> 
								</li>
								@endif
							</ul>
						</li>	
					</ul>
					<div class="clearfix"> </div>
				</div>
				<!--notification menu end -->
				<div class="profile_details">		
					<ul>
						<li class="dropdown profile_details_drop">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<div class="profile_img">	
									<!-- <span class="prfil-img"><img src="{{ asset('images/2.png')}}" alt=""> </span>  -->
									<div class="user-name">
										<p>{{ Auth::user()->username }}</p>
										<span>{{ Auth::user()->role }}</span>

									</div>
									<i class="fa fa-angle-down lnr"></i>
									<i class="fa fa-angle-up lnr"></i>
									<div class="clearfix"></div>	
								</div>	
							</a>
							<ul class="dropdown-menu drp-mnu">
								<!-- <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li>  -->
								<li> <a href="{{  route('admin.user.inforUserr')}}"><i class="fa fa-user"></i> Profile</a> </li> 
								<li> <a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Đăng xuất</a> </li>
							</ul>
						</li>
					</ul>
				</div>	      
				<div class="clearfix"> </div>	
			</div>
			<div class="clearfix"> </div>	
		</div>
		<!-- //header-ends -->
