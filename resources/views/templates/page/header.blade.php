<!DOCTYPE html>
<html lang="zxx">

<head>
	<title>@yield('title')</title>
	<!--/tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Grocery Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('css/style2.css') }}" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
	<!--pop-up-box-->
	<link href="{{ asset('css/popuo-box.css') }}" rel="stylesheet" type="text/css" media="all" />
	<!--//pop-up-box-->
	<!-- price range -->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui1.css') }}">
	<!-- fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">

</head>

<body>
	<!-- top-header -->
	<!-- //top-header -->
	<!-- header-bot-->
	<div class="header-bot">
		<div class="header-bot_inner_wthreeinfo_header_mid row">
			<!-- header-bot-->
			<div class="col-md-4 logo_agile">
				<h1>
					<a href="/">
						
						<span>Không Gian Xanh</span>
						<!-- <img src="{{ asset('images/logo2.png')}}" alt=" "> -->
					</a>
				</h1>
			</div>
			<!-- header-bot -->
			<div class="col-md-8 header">
				<!-- header lists -->
				<ul>
					
					@if (!Auth::check())
					<li>
						<a href="{{ route('page.login.login')}}" >
							<span class="fa fa-unlock-alt" aria-hidden="true"></span> Đăng nhập 
						</a>
					</li>
					<li>
						<a href="{{ route('page.login.register')}}" >
							<span class="fa fa-pencil-square-o" aria-hidden="true"></span> Đăng ký 
						</a>
					</li>
					@else
					<li>
						<a href="" >
							<span class="fa fa-unlock-alt" aria-hidden="true"></span>{{Auth::user()->fullname}}
						</a>
					</li>
					<li>
						<a href="{{ route('page.login.logout')}}" >
							<span class="fa fa-pencil-square-o" aria-hidden="true"></span>Đăng xuất
						</a>
					</li>
					@endif

				</ul>
				<!-- //header lists -->
				<!-- search -->
				<div class="agileits_search">
					<form action="{{ route('page.index.search')}}" method="get" class="last">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Tìm kiếm"  name="search" id="tukhoa">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit">
									<i class="glyphicon glyphicon-search"></i>
								</button>
							</div>
						</div>
					</form>

				</div>
				<div class="top_nav_right">
					<div class="wthreecartaits wthreecartaits2 box_1 cart">
						<a href="{{route('page.index.cart')}}" id="cart-shop"  style="background: #299e32;padding: 8px; color: #fff; position: fixed; margin-left: 50px;">
							<i class="fa fa-cart-arrow-down" aria-hidden="true" style="color: #fff;"></i>
							<span  id="count-item" data-count="{{ Cart::count()}}"> 
								{{ Cart::count()}}
							</span>
						</a>	

					</div>
				</div>
				<!-- //cart details -->
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="ban-top">
			<div class="container">
				<div class="top_nav_left">
					<nav class="navbar navbar-default">
						<div class="container-fluid">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
								aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav menu__list">
								<li>
									<a class="nav-stylehead" href="/">Trang chủ
										<span class="sr-only">(current)</span>
									</a>
								</li>
								<li class="">
									<a class="nav-stylehead" href="about.html">Giới thiệu</a>
								</li>
								<li class="allcat"><a href="">Tất cả các danh mục</a>
									<i class="fa fa-angle-down" style="color: #fff"></i>
									<ul class="alltype nav">
										@foreach($objCategory as $oItem)
										<li class="item-cat" xuan="{{ $oItem->id }}">
											<i class="fa fa-angle-right float-right"></i>
											<a class="float" href="">{{ $oItem->name}}</a>
											
											<ul class="pros">
												@foreach($objProType as $value)
												@php
												$name = $value->name;
												$name_slug = str_slug($name,'-');
												$urlProType = route('page.index.productType',['id' => $value->id, 'name' => $name_slug]);
												@endphp
												@if($oItem->id === $value->id_cat)
												<li><a class="float" href="{{$urlProType}}">{{ $value->name}}</a></li>
												@endif
												@endforeach
											</ul>
										</li>

										@endforeach
									</ul>
								</li>

								<li>
									<a class="" href="contact.html">Liên hệ</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="page-head_agile_info_w3l">

</div>
