<!-- product left -->
<div class="side-bar col-md-3">
	<div class="search-hotel">
		<h3 class="agileits-sear-head">Tìm kiếm...</h3>
		<form action="{{ route('page.index.searchPro')}}" method="get">
			{{ csrf_field()}}
			<input type="search" placeholder="Tên sản phẩm..." name="search" />
			<input type="submit" value="  ">
		</form>
	</div>
	<!-- price range -->
	<div class="range">
		<h3 class="agileits-sear-head">Mức giá</h3>
		<select name="price_range" id="price_range" class="form-control">
			<option value="">--Chọn--</option>
			<option value="1000000">Dưới 1.000.000</option>
			<option value="2000000">Dưới 2.000.000</option>
			<option value="3000000">Dưới 3.000.000</option>
			<option value="4000000">Dưới 4.000.000</option>
			<option value="5000000">Dưới 5.000.000</option>
			<option value="6000000">Dưới 6.000.000</option>
			<option value="7000000">Dưới 7.000.000</option>
			<option value="8000000">Dưới 8.000.000</option>
			<option value="9000000">Dưới 9.000.000</option>
			<option value="10000000">Dưới 10.000.000</option>
			<option value="10000001">Trên 10.000.000</option>
			
		</select>
	</div>
	<!-- //price range -->
	
	<!-- discounts -->
	<div class="left-side">
		<h3 class="agileits-sear-head">Giảm giá</h3>
		<select name="discounts" id="discounts" class="form-control">
			<option value="">--Chọn--</option>
			<option value="5">Dưới 5%</option>
			<option value="10">Dưới 10%</option>
			<option value="20">Dưới 20%</option>
			<option value="30">Dưới 30%</option>
			<option value="40">Dưới 40%</option>
			<option value="50">Dưới 50%</option>
			<option value="60">Dưới 60%</option>
			<option value="61">Trên 60%</option>
			
		</select>
	</div>
	<!-- //discounts -->

	<!-- deals -->
	<div class="deal-leftmk left-side">
		<h3 class="agileits-sear-head">Ưu đãi đặc biệt</h3>
		@foreach($objPro as $value)
		@php
		if($value->sale > 0){
		$price = $value->price * $value->sale/100;
	}
	else{
	$price = $value->price;
}
$name = $value->name;
$name_Slug = str_slug($name,'-');
$urlDetail = route('page.index.detail',['id' => $value->id, 'name' =>$name_Slug ]);
@endphp
<div class="special-sec1">
	<div class="col-xs-4 img-deals" >
		<img src="{{ asset('files/'.$value->product_details->first()->picture) }}" alt="" style="width: 100%">
	</div>
	<div class="col-xs-8 img-deal1">
		<a href="{{$urlDetail}}" style="font-size: 13px;">{{ str_limit($value->name,30) }}</a></br>
		<a href="{{$urlDetail}}">{{ number_format($price,0,",",".") }}đ</a>
	</div>
	<div class="clearfix"></div>
</div>
@endforeach

</div>
<!-- //deals -->
</div>
			<!-- //product left -->