@include('templates.page.header')
<div class="ads-grid">
	<div class="container">
		@include('templates.page.navbar_left')
		@yield('content')
	</div>
</div>
@include('templates.page.footer')
@yield('js')