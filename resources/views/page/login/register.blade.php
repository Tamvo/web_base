@extends('templates.page.master')
@section('title')
Đăng Ký
@stop
@section('content')
<div class="agileinfo-ads-display col-md-9 w3l-rightpro">
	<div class="wrapper">
		<!-- 2nd section) -->
		<h3 class="tittle-w3l">Đăng Ký
			<span class="heading-style">
				<i></i>
				<i></i>
				<i></i>
			</span>
		</h3>
		@if(Session::has('msg'))
		<p class="alert alert-success">{{ Session::get('msg') }}</p>
		
		@endif
		@if(Session::has('error'))
		<p class="alert alert-danger">{{ Session::get('error') }}</p>
		
		@endif
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<div class="modal-content">
			<div class="modal-body modal-body-sub_agile">
				<div class="modal_body_left modal_body_left1">
					<form action="{{ route('page.login.register')}}" method="post">
						{{ csrf_field()}}
						<div class="styled-input agile-styled-input-top">
							<input type="text" placeholder="Tên đăng nhập" name="username" >
						</div>
						<div class="styled-input agile-styled-input-top">
							<input type="text" placeholder="Họ Tên" name="fullname" >
						</div>
						<div class="styled-input">
							<input type="email" placeholder="E-mail" name="email" >
						</div>
						<div class="styled-input">
							<input type="password" placeholder="Password" name="password" id="password1" >
						</div>
						<div class="styled-input">
							<input type="password" placeholder="Confirm Password" name="confirm-password" id="password2" >
						</div>
						<input type="submit" value="Đăng ký">
					</form>
				</div>
			</div>
		</div>
		<!-- //Modal content-->
	</div>
		<!-- </div>
		
	</div>
-->
</div>
</div>
@stop
