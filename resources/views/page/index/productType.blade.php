@extends('templates.page.master')
@section('title')
Trang chủ
@stop
@section('content')
<div class="agileinfo-ads-display col-md-9 w3l-rightpro">
	<div class="wrapper">
		<!-- 2nd section) -->
		<h3 class="tittle-w3l">{{ $nameType->name}}
			<span class="heading-style">
				<i></i>
				<i></i>
				<i></i>
			</span>
		</h3>
		@if(Session::has('msg'))
		<p class="alert alert-success">{{ Session::get('msg') }}</p>
		
		@endif
		@if(Session::has('error'))
		<p class="alert alert-danger">{{ Session::get('error') }}</p>
		
		@endif
		<div class="product-sec1">
			@if(count($data) === 0)
			<div class="title text-center" style="margin: 4em 4em">
				<h4>Sản Phẩm bạn đang tìm hiện tại chưa cập nhập</h4>
				<span>Xin lỗi, chúng tôi không thể tìm được kết quả hợp với tìm kiếm của bạn</span>
			</div>
			@endif
			@foreach($data as $iTem)
			@php 
			$name = $iTem->name;
			$name_Slug = str_slug($name,'-');
			$urlDetail = route('page.index.detail',['id' => $iTem->id, 'name' =>$name_Slug ]);
			@endphp

			<div class="col-md-4 product-men">
				<div class="men-pro-item simpleCart_shelfItem">
					<div class="men-thumb-item">
						<img src="{{ asset('files/'.$iTem->product_details->first()->picture) }}" style="width: 100%;height: 100%" alt="">
						<div class="men-cart-pro">
							<div class="inner-men-cart-pro">
								<a href="{{$urlDetail}}" class="link-product-add-cart">Xem chi tiết</a>
							</div>
						</div>
						@if($iTem->sale != 0)
						<span class="product-new-top">-{{$iTem->sale }} %</span>
						@endif
					</div>
					<div class="item-info-product ">
						<h4>
							<a href="{{$urlDetail}}">{{ str_limit($iTem->name,40) }}</a>
						</h4>
						<div class="info-product-price">
							@if($iTem->sale != 0)
							<span class="item_price">{{ number_format(round($iTem->price - ($iTem->price * $iTem->sale/100),-3),0,",",".")}}đ</span>
							<del>{{ number_format($iTem->price,0,",",".") }}đ</del>
							@else
							<span class="item_price">{{ number_format(round($iTem->price,-3),0,",",".")}}đ</span>
							@endif
						</div>

					</div>
					<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
						<a class="add-to-cart pull-left" id="addcart" xuan1="{{$iTem->id}}" href="javascript:void(0);"  role="button"  >
							Thêm vào giỏ hàng
							
						</a>
					</div>

				</div>
			</div>
			@endforeach
			<div class="clearfix"></div>
		</div>
		
		
	</div>
	<div class="row" style="color: red">{{ $data->links() }}</div>
</div>
</div>
@stop
@section('js')
<script type="text/javascript">
	$('#price_range').on('change',function(){
		let price = $('#price_range').val();
		$.ajax({
			type:'GET',
			url: '{{route('page.index.searchPrice')}}',
			data: {price: price},
			success: function(data){
				// alert(data);
				$('.product-sec1').html(data);
			}
		});
	});
	
</script>
<script type="text/javascript">
	$('#discounts').on('change',function(){
		let sale = $('#discounts').val();
		$.ajax({
			type:'GET',
			url: '{{route('page.index.searchSale')}}',
			data: {sale: sale},
			success: function(data){
				// alert(data);
				$('.product-sec1').html(data);
			}
		});
	});
	
</script>


<script type="text/javascript">
	$(document).ready(function($){
		$('.add-to-cart').click(function(){
			var pro_id =$(this).attr('xuan1');
			// alert(pro_id);
			$.ajax({
				url : '{{route('page.index.shopping')}}',
				type:'GET',
				data:{id:pro_id},
				success: function(data){
					// console.log(data);
					
					// alert(data);
				}

			});

			
		});
	});



	$(document).ready(function($){
		$('.add-to-cart').on('click',function(){
			$(document).find('.add-to-cart').addClass('disable');
			var cart =$(document).find('#cart-shop');
			var citem = parseInt(cart.find('#count-item').data('count')) + 1;
			cart.find('#count-item').text( citem).data('count', citem);
			
		});
	});
</script>
@stop