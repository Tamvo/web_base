@include('templates.page.header')


<div class="privacy">
	<div class="container">
		
		<!-- tittle heading -->
		<h3 class="tittle-w3l">Giỏ hàng
			<span class="heading-style">
				<i></i>
				<i></i>
				<i></i>
			</span>
		</h3>
		<!-- //tittle heading -->
		<div class="checkout-right">
			<h4 class="text-center">Giỏ hàng của bạn có
				<span>{{ Cart::count()}} sản phẩm</span>
			</h4>
			@if(Cart::count()!= 0)
			<div class="table-responsive">
				<table class="timetable_sub">
					<thead>
						<tr>
							<th>STT</th>
							<th>Hình ảnh</th>
							<th>Số lượng</th>
							<th>Tên sản phẩm</th>
							<th>Giá sản phẩm</th>
							<th>Tổng tiền</th>
							<th>Xóa</th>
						</tr>
					</thead>
					<tbody>
						@foreach($cartItems as $cartItem)
						<tr class="rem1">
							<td class="invert">1</td>
							<td class="invert-image" style="width: 200px;">
								<a href="">
									<img src="{{ asset('files/'.$cartItem->options->picture)}}">
								</a>
							</td>
							<td class="invert">
								<div>
									<div class="quantity-select">
										
										<input type="number" class="quantity" rowIdUD="{{ $cartItem->rowId}}" sl="{{ $cartItem->options->quantity }}" style="text-align:center; max-width:50px; " min="1" value="{{$cartItem->qty}}" />
									</div>
								</div>
							</td>
							<td class="invert">{{$cartItem->name}}</td>
							<td class="invert">{{number_format($cartItem->price,0,",",".")}}</td>
							<td class="invert">{{number_format($cartItem->price * $cartItem->qty,0,",",".")}}</td>
							<td class="invert">
								<div class="rem">
									<a class="cart_quantity_delete" href="javascript:void(0)" delete ="{{ $cartItem->rowId}}"><i class="fa fa-trash" aria-hidden="true"></i></a>
								</div>
							</td>
						</tr>
						@endforeach
						<tr>
							<td colspan="5" class="text-left">Tổng tiền</td>
							<td>{{ Cart::subtotal() }}</td>
						</tr>

					</table>
					<div class="ckeckcart">
						<h5><a href="{{ route('page.index.tocheckout')}}">Xác nhận giỏ hàng</a></h5>
					</div>
				</div>
				@endif
			</div>
			
		</div>
	</div>

	<!-- //checkout page -->
	<!-- newsletter -->

	@include('templates.page.footer')

	<script type="text/javascript">
		$(document).ready(function(){
			$('.cart_quantity_delete').on('click', function(){
				let rowId = $(this).attr('delete');
				// alert(rowId);
				$.ajax({
					type:'get',
					url: '{{route('page.index.delete')}}',
					data: {rowId: rowId},
					success: function(data){
						window.location ="{{route('page.index.cart')}}";
					}
				});
			});
		});
	</script>


	<script type="text/javascript">
		$(document).ready(function(){
			$('.quantity').on('change', function(){
				let qty = $(this).val();
				let rowId = $(this).attr('rowIdUD');
				let quantity = $(this).attr('sl');
				
				if(parseInt(qty) > parseInt(quantity))
				{
					alert('Bạn không được mua quá ' + quantity + ' sản phẩm');
					$(this).val(quantity);
				}
				else{
					$.ajax({
						type:'get',
						url: '{{route('page.index.update')}}',
						data: {rowId: rowId, qty: qty},
						success: function(data){
							window.location ="{{route('page.index.cart')}}";
						}
					});
				}
			});
		});
	</script>
