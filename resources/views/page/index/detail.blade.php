@include('templates.page.header')
@section('title')
Chi tiết sản phâm
@stop
<!-- Single Page -->
<div class="banner-bootom-w3-agileits">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h1 class="text-capitalize" style="margin-left: 25px; margin-bottom: 20px">{{ $arr->name }}</h1>
				<div class="col-md-12">
					<div class="col-md-5 single-right-left">
						<div class="image-prodcut" >
							<img src="{{ asset('files/'.$arr->product_details->first()->picture) }}"  alt="" style="width: 100%; height:100%;" >
						</div>
						<div class="container-fluid">
							<div class="row">
								@foreach($arr->product_details as $photo)
								<div class="col-md-2" style="padding: 0px; height: 60px;">
									<img src="{{ asset('files/'.$photo->picture) }}" class="image-products" alt="" style="width: 100%; height: 100%; border: 1px solid #eee;" />
								</div>
								@endforeach
							</div>
							<div class="clearfix"></div>
						</div>
						
					</div>
					<div class="col-md-7 single-right-left simpleCart_shelfItem">
						<span><b>Tình trạng</b>: Còn {{ $arr->quantity }} sản phẩm</span><br>
						<span><b>Đơn giá</b>:
							@if($arr->sale != 0)
							<span class="item_price">{{ number_format(round($arr->price - ($arr->price * $arr->sale/100),-3),0,",",".")}}đ</span>
							<del>{{ number_format($arr->price,0,",",".") }}đ</del>
							@else
							<span class="item_price">{{ number_format(round($arr->price,-3),0,",",".")}}đ</span>
							@endif
						</span><br>
						<span><b>Mô tả ngắn</b></span>
						<div class="single-infoagile">
							{!! str_limit($arr->discription,250) !!}
						</div>
						<div class="col-md-12">
							<div class="occasion-cart" style="margin-top: 20px;">
								<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out" style="width: 100%">
									<a class="add-to-cart" style="width: 50%;" id="addcart" xuan1="{{$arr->id}}" href="javascript:void(0);"  role="button"  >
										Thêm vào giỏ hàng
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-12" style="margin-top: 20px">
					<span class="product-detail">Mô tả sản phẩm</span>
					<div style="text-align: justify; margin-top: 15px; margin-left: 12px">{!! $arr->discription !!}</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="deal-leftmk left-side">
					<h3 class="agileits-sear-head">Ưu đãi đặc biệt</h3>
					@foreach($objPro as $value)
					@php
					$price =$value->price * $value->sale/100;
					$name = $value->name;
					$name_Slug = str_slug($name,'-');
					$urlDetail = route('page.index.detail',['id' => $value->id, 'name' =>$name_Slug ]);
					@endphp
					<div class="special-sec1">
						<div class="col-xs-4 img-deals" >
							<img src="{{ asset('files/'.$value->product_details->first()->picture) }}" alt="" style="width: 100%">
						</div>
						<div class="col-xs-8 img-deal1">
							<a href="{{$urlDetail}}" style="font-size: 13px;">{{ str_limit($value->name,30) }}</a></br>
							<a href="{{$urlDetail}}">{{ number_format($price,0,",",".") }}đ</a>
						</div>
						<div class="clearfix"></div>
					</div>
					@endforeach

				</div>
			</div>
		</div>

	</div>
</div>
<!-- //Single Page -->
<!-- special offers -->
<div class="featured-section" id="projects">
	<div class="container">
		<!-- tittle heading -->
		<h3 class="tittle-w3l">Sản phẩm liên quan</h3>
		<!-- //tittle heading -->
		<div class="content-bottom-in">
			<ul id="flexiselDemo1">
				@foreach($proTypes as $product )
				@php 
				$name = $product->name;
				$name_Slug = str_slug($name,'-');
				$urlDetail = route('page.index.detail',['id' => $product->id, 'name' =>$name_Slug ]);
				@endphp
				<li>
					<div class="w3l-specilamk" >
						<div class="men-thumb-item">
							<img src="{{ asset('files/'.$product->product_details->first()->picture) }}" style="width: 100%;height: 100%" alt="">
							<div class="men-cart-pro">
								<div class="inner-men-cart-pro">
									<a href="{{$urlDetail}}" class="link-product-add-cart">Xem chi tiết</a>
								</div>
							</div>
							@if($product->sale != 0)
							<span  class="product-new-top">-{{$product->sale }} %</span>
							@endif
						</div>
						
						<div class="product-name-w3l">
							<h4>
								<a href="{{$urlDetail}}">{{  str_limit($product->name,20) }}</a>
							</h4>
							<div class="w3l-pricehkj">
								<h6>{{ number_format(round($product->price - ($product->price * $product->sale/100),-3),0,",",".")}}đ</h6>
								<p style="text-decoration: line-through;">{{ number_format($product->price,0,",",".") }}đ</p>
							</div>
							<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
								<a class="add-to-cart" style="width: 100%;" id="addcart" xuan1="{{$product->id}}" href="javascript:void(0);"  role="button"  >
									Thêm vào giỏ hàng

								</a>
								
								
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>
<!-- //special offers -->

@include('templates.page.footer')

<script type="text/javascript">
	$(document).ready(function($){
		$('.add-to-cart').click(function(){
			var pro_id =$(this).attr('xuan1');
			// alert(pro_id);
			$.ajax({
				url : '{{route('page.index.shopping')}}',
				type:'GET',
				data:{id:pro_id},
				success: function(data){
					// console.log(data);
					
					// alert(data);
				}

			});

			
		});
	});



	$(document).ready(function($){
		$('.add-to-cart').on('click',function(){
			$(document).find('.add-to-cart').addClass('disable');
			var cart =$(document).find('#cart-shop');
			var citem = parseInt(cart.find('#count-item').data('count')) + 1;
			cart.find('#count-item').text( citem).data('count', citem);
			
		});
	});

	$('.image-products').on('click', function(){
		
		let src = $(this).attr('src');
		let src1 = $('.image-prodcut img').attr('src');
		
		if(src != src1) { 
			$('.image-prodcut img').attr('src',src);         
		}
	});
</script>
