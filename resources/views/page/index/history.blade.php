@extends('templates.page.master')
@section('title')
Trang chủ
@stop
@section('content')
<div class="agileinfo-ads-display col-md-9 w3l-rightpro">
	<div class="wrapper">
		<h3 class="tittle-w3l">Lịch sử mua hàng
			<span class="heading-style">
				<i></i>
				<i></i>
				<i></i>
			</span>
		</h3>
		<table class="table">
			<thead>
				<tr>
					<th>Ngày đặt hàng</th>
					<th>Mã sản phẩm</th>
					<th>Số lượng</th>
					<th>Đơn giá</th>
					<th>Tổng tiền</th>
				</tr>
			</thead>
			<tbody>

				@foreach($data as $oItem)
				@foreach($oItem->billDetails as $value)
				<tr>
					<th scope="row">{{ $oItem->date_order }}</th>
					<td>{{ $value->id_product }}</td>
					<td>{{$value->quantity}}</td>
					<td>{{number_format($value->price,0,",",".")}}đ</td>
					<td>{{number_format($value->price *$value->quantity ,0,",",".")}}đ</td>
					
					
				</tr>
				@endforeach
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop
@section('js')
<script type="text/javascript">
	$('#price_range').on('change',function(){
		let price = $('#price_range').val();
		$.ajax({
			type:'GET',
			url: '{{route('page.index.searchPrice')}}',
			data: {price: price},
			success: function(data){
				// alert(data);
				$('.product-sec1').html(data);
			}
		});
	});
	
</script>
<script type="text/javascript">
	$('#discounts').on('change',function(){
		let sale = $('#discounts').val();
		$.ajax({
			type:'GET',
			url: '{{route('page.index.searchSale')}}',
			data: {sale: sale},
			success: function(data){
				// alert(data);
				$('.product-sec1').html(data);
			}
		});
	});
	
</script>


<script type="text/javascript">
	$(document).ready(function($){
		$('.add-to-cart').click(function(){
			var pro_id =$(this).attr('xuan1');
			// alert(pro_id);
			$.ajax({
				url : '{{route('page.index.shopping')}}',
				type:'GET',
				data:{id:pro_id},
				success: function(data){
					// console.log(data);
					
					// alert(data);
				}

			});

			
		});
	});



	$(document).ready(function($){
		$('.add-to-cart').on('click',function(){
			$(document).find('.add-to-cart').addClass('disable');
			var cart =$(document).find('#cart-shop');
			var citem = parseInt(cart.find('#count-item').data('count')) + 1;
			cart.find('#count-item').text( citem).data('count', citem);
			
		});
	});
</script>
@stop