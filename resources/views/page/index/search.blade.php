@extends('templates.page.master')
@section('content')
<div class="agileinfo-ads-display col-md-9 w3l-rightpro">
	<div class="wrapper">
		<!-- 2nd section) -->
		<p>{{ count($data)}} mặt hàng được tìm thấy theo "{{ $search }}"</p>
		@if(count($data)  == 0)
		<div class="title text-center" style="margin: 4em 4em">
			<h4>Tìm kiếm không có kết quả</h4>
			<span>Xin lỗi, chúng tôi không thể tìm được kết quả hợp với tìm kiếm của bạn</span>
		</div>
		@else
		<div class="product-sec1">
			@foreach($data as $iTem)
			
			<div class="col-md-4 product-men">
				<div class="men-pro-item simpleCart_shelfItem">
					<div class="men-thumb-item">
						<img src="{{ asset('files/'.$iTem->product_details->first()->picture) }}" style="width: 100%;height: 100%" alt="">
						<div class="men-cart-pro">
							<div class="inner-men-cart-pro">
								<a href="single.html" class="link-product-add-cart">Xem chi tiết</a>
							</div>
						</div>
						@if($iTem->sale != 0)
						<span class="product-new-top">-{{$iTem->sale }} %</span>
						@endif
					</div>
					<div class="item-info-product ">
						<h4>
							<a href="single.html">{{ str_limit($iTem->name,40) }}</a>
						</h4>
						<div class="info-product-price">
							@if($iTem->sale != 0)
							<span class="item_price">{{ number_format(round($iTem->price - ($iTem->price * $iTem->sale/100),-3),0,",",".")}}đ</span>
							<del>{{ number_format($iTem->price,0,",",".") }}đ</del>
							@else
							<span class="item_price">{{ number_format(round($iTem->price,-3),0,",",".")}}đ</span>
							@endif
						</div>
						<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
							<a class="add-to-cart" style="width: 100%;" id="addcart" xuan1="{{$iTem->id}}" href="javascript:void(0);"  role="button"  >
								Thêm vào giỏ hàng

							</a>
						</div>

					</div>
				</div>
			</div>
			@endforeach
			<div class="clearfix"></div>
		</div>
		@endif
		<!-- <div id="ad">
			hhh
		</div>
	-->
</div>
</div>
@stop
@section('js')
<script type="text/javascript">
	$(document).ready(function($){
		$('.add-to-cart').click(function(){
			var pro_id =$(this).attr('xuan1');
			// alert(pro_id);
			$.ajax({
				url : '{{route('page.index.shopping')}}',
				type:'GET',
				data:{id:pro_id},
				success: function(data){
					// console.log(data);
					
					// alert(data);
				}

			});

			
		});
	});



	$(document).ready(function($){
		$('.add-to-cart').on('click',function(){
			$(document).find('.add-to-cart').addClass('disable');
			var cart =$(document).find('#cart-shop');
			var citem = parseInt(cart.find('#count-item').data('count')) + 1;
			cart.find('#count-item').text( citem).data('count', citem);
			
		});
	});
</script>
@stop
