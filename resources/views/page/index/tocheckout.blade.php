@include('templates.page.header')


<div class="privacy">
	<div class="container" style="background-color: #eee;">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<form action="{{ route('page.index.tocheckout')}}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="row">
				<div class="col-md-8  information " style="border-right: 2px solid #fff;">
					<div class="container-fluid" >
						@if(Auth::check())
						<h4>Thông tin giao hàng</h4>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Tên</label>
									<input type="text" class="form-control border-input" name="fullname" placeholder="Họ Tên" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Địa chỉ nhận hàng</label>
									<input type="text" class="form-control border-input" name="address" placeholder="Địa chỉ nhận hàng" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Số điện thoại</label>
									<input type="text" class="form-control border-input" name="phone" placeholder="Số điện thoại người nhận hàng" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Ghi chú</label>
									<input type="text" class="form-control border-input" name="note" placeholder="Số điện thoại người nhận hàng" />
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="col-md-4">
					<div class="container-fluid" >
						<div class="row">
							<div class="col-md-12">
								<h4 class="xacnhan" style="width: 100%;">Thanh toán</h4>
							</div>
						</div>
						<div class="row ">
							<div class="col-md-12">
								<h4 class="tocart">Thông tin đơn hàng</h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<h5 class="tocart">Tạm tính ({{ Cart::count()}} sản phẩm)</h5>
							</div>
							<div class="col-md-4">
								<h5 class="tocart">{{ Cart::subtotal() }}</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8">
								<h5 class="tocart">Tổng cộng</h5>
							</div>
							<div class="col-md-4">
								<h5 class="tocart">{{ Cart::subtotal() }}</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="submit" class="btn btn-primary" value="Thanh Toán">
							</div>
						</div>
						@else
						<h4 class="agileinfo_sign"> <a href="{{ Route('page.login.login')}}">Vui lòng đăng nhập</a></h4>
						@endif
					</div>
				</div>
			</div>
		</form>
		<div class="clearfix"></div>

	</div>

</div>
</div>

<!-- //checkout page -->
<!-- newsletter -->

@include('templates.page.footer')
