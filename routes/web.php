<?php
Route::pattern('id', '([0-9]*)');
Route::pattern('name', '(.*)');

// Route::get('/', function () {
// 	return view('welcome');
// });
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function(){
	Route::get('index',[
		'uses' => 'IndexController@index',
		'as' => 'admin.index.index'
	]);

	Route::group(['prefix' => 'cat'], function(){
		Route::get('index',[
			'uses' => 'CatController@index',
			'as' => 'admin.category.index'
		])->middleware('test: admin|mod');
		Route::get('add',[
			'uses' => 'CatController@getAdd',
			'as' => 'admin.category.add'
		])->middleware('test: admin|mod');
		Route::post('add',[
			'uses' => 'CatController@postAdd',
			'as' => 'admin.category.add'
		])->middleware('test: admin|mod');
		Route::get('update/{id}',[
			'uses' => 'CatController@getUpdate',
			'as' => 'admin.category.update'
		])->middleware('test: admin|mod');
		Route::post('update/{id}',[
			'uses' => 'CatController@postUpdate',
			'as' => 'admin.category.update'
		])->middleware('test: admin|mod');
		Route::get('delete/{id}',[
			'uses' => 'CatController@delete',
			'as' => 'admin.category.delete'
		])->middleware('test: admin|mod');

	});
	
	Route::group(['prefix' => 'producttype'], function(){
		Route::get('index',[
			'uses' => 'ProductTypeController@index',
			'as' => 'admin.productType.index'
		])->middleware('test: admin|mod');
		Route::get('add',[
			'uses' => 'ProductTypeController@getAdd',
			'as' => 'admin.productType.add'
		])->middleware('test: admin|mod');
		Route::post('add',[
			'uses' => 'ProductTypeController@postAdd',
			'as' => 'admin.productType.add'
		])->middleware('test: admin|mod');
		Route::get('update/{id}',[
			'uses' => 'ProductTypeController@getUpdate',
			'as' => 'admin.productType.update'
		])->middleware('test: admin|mod');
		Route::post('update/{id}',[
			'uses' => 'ProductTypeController@postUpdate',
			'as' => 'admin.productType.update'
		])->middleware('test: admin|mod');
		Route::get('delete/{id}',[
			'uses' => 'ProductTypeController@delete',
			'as' => 'admin.productType.delete'
		])->middleware('test: admin|mod');

	});

	Route::group(['prefix' => 'user'], function(){
		Route::get('index',[
			'uses' => 'UserController@index',
			'as' => 'admin.user.index'
		])->middleware('test: admin');
		Route::get('add',[
			'uses' => 'UserController@getAdd',
			'as' => 'admin.user.add'
		])->middleware('test: admin');
		Route::post('add',[
			'uses' => 'UserController@postAdd',
			'as' => 'admin.user.add'
		])->middleware('test: admin');
		Route::get('update/{id}',[
			'uses' => 'UserController@getUpdate',
			'as' => 'admin.user.update'
		])->middleware('test: admin');
		Route::post('update/{id}',[
			'uses' => 'UserController@postUpdate',
			'as' => 'admin.user.update'
		])->middleware('test: admin');
		Route::get('delete/{id}',[
			'uses' => 'UserController@delete',
			'as' => 'admin.user.delete'
		])->middleware('test: admin');

		Route::get('infor',[
			'uses' => 'UserController@infor',
			'as' =>'admin.user.inforUserr'
		]);
		Route::post('addInfor',[
			'uses' => 'UserController@addInfor',
			'as' =>'admin.user.inforUser'
		]);

	});
	Route::group(['prefix' => 'customer'], function(){
		Route::get('index',[
			'uses' => 'CustomerController@index',
			'as' => 'admin.customer.index'
		]);

		Route::get('delete/{id}',[
			'uses' => 'CustomerController@delete',
			'as' => 'admin.customer.delete'
		]);
		


	});

	Route::group(['prefix' => 'cusdetail'], function(){
		Route::get('index',[
			'uses' => 'CusDetailController@index',
			'as' => 'admin.customerdetail.index'
		]);
		Route::get('detail',[
			'uses' => 'CusDetailController@detail',
			'as' => 'admin.customerdetail.detail'
		]);
		Route::get('delete/{id}',[
			'uses' => 'CusDetailController@delete',
			'as' => 'admin.customerdetail.delete'
		]);
		


	});
	Route::group(['prefix' => 'product'], function(){
		Route::get('index',[
			'uses' => 'ProductController@index',
			'as' => 'admin.product.index'
		])->middleware('test: admin|mod');

		Route::get('add',[
			'uses' => 'ProductController@getAdd',
			'as' => 'admin.product.add'
		])->middleware('test: admin|mod');

		Route::post('add',[
			'uses' => 'ProductController@postAdd',
			'as' => 'admin.product.add'
		])->middleware('test: admin|mod');

		Route::get('update/{id}',[
			'uses' => 'ProductController@getUpdate',
			'as' => 'admin.product.update'
		])->middleware('test: admin|mod');

		Route::post('update/{id}',[
			'uses' => 'ProductController@postUpdate',
			'as' => 'admin.product.update'
		])->middleware('test: admin|mod');

		Route::get('delete/{id}',[
			'uses' => 'ProductController@delete',
			'as' => 'admin.product.delete'
		])->middleware('test: admin|mod');

		Route::get('change',[
			'uses' => 'ProductController@change',
			'as' => 'admin.product.change'
		])->middleware('test: admin|mod');

		Route::get('updateqty', [
			'uses' => 'ProductController@updateQty',
			'as' => 'admin.product.qty'
		])->middleware('test: admin|mod');

		Route::get('active', [
			'uses' => 'ProductController@active',
			'as' => 'admin.product.active'
		])->middleware('test: admin|mod');

	});

	Route::group(['prefix' => 'contact'], function(){
		Route::get('index',[
			'uses' => 'ContactController@index',
			'as' => 'admin.contact.index'
		]);
		
		Route::get('show',[
			'uses' => 'ContactController@show',
			'as' => 'admin.contact.show'
		]);
		
		Route::delete('delete',[
			'uses' => 'ContactController@delete',
			'as' => 'admin.contact.delete'
		]);

	});

	Route::group(['prefix' => 'bill'], function(){
		Route::get('index',[
			'uses' => 'BillController@index',
			'as' => 'admin.bill.index'
		])->middleware('test: admin|mod');
		
		Route::get('delete/{id}',[
			'uses' => 'BillController@del',
			'as' => 'admin.bill.del'
		])->middleware('test: admin|mod');
		Route::get('active',[
			'uses' => 'BillController@active',
			'as' =>'admin.bill.active'
		])->middleware('test: admin|mod');
		Route::get('bill-user',[
			'uses' => 'BillController@billUser',
			'as' =>'admin.bill.billuser'
		]);
	});


	Route::group(['prefix' => 'billdetail'], function(){
		Route::get('index/{id}',[
			'uses' => 'BillDetailController@index',
			'as' => 'admin.billdetail.index'
		]);

		Route::get('delete/{id}',[
			'uses' => 'BillDetailController@del',
			'as' => 'admin.billdetail.del'
		])->middleware('test: admin|mod');

		Route::get('createHD/{id}',[
			'uses' => 'BillDetailController@create',
			'as' => 'admin.billdetail.creatHD'
		]);

		Route::get('active',[
			'uses' => 'BillDetailController@active',
			'as' =>'admin.billdetail.active'
		]);
	});
});


Route::group(['namespace' => 'Auth'], function(){
	Route::get('login',[
		'uses' =>'AuthController@getLogin',
		'as' => 'login'
	]);
	Route::post('login',[
		'uses' =>'AuthController@postLogin',
		'as' => 'auth.login'
	]);
	Route::get('logout',[
		'uses' =>'AuthController@logout',
		'as' => 'logout'
	]);
	
});

Route::group(['namespace' => 'Page'], function(){
	Route::get('/',[
		'uses' => 'IndexController@index',
		'as' =>'page.index.index'
	]);

	Route::get('{name}-{id}.html',[
		'uses' => 'DetailController@index',
		'as'  => 'page.index.detail'
	]);

	Route::get('{name}-{id}',[
		'uses' => 'ProductTypeController@index',
		'as' => 'page.index.productType'
	]);

	Route::get('dang-nhap',[
		'uses' => 'UserController@getLogin',
		'as' =>'page.login.login'
	]);

	Route::post('dang-nhap',[
		'uses' => 'UserController@postLogin',
		'as' =>'page.login.login'
	]);

	Route::get('dang-ky',[
		'uses' => 'UserController@getRegister',
		'as' =>'page.login.register'
	]);

	Route::post('dang-ky',[
		'uses' => 'UserController@postRegister',
		'as' =>'page.login.register'
	]);

	Route::get('dang-xuat',[
		'uses' => 'UserController@logout',
		'as' => 'page.login.logout'
	]);

	Route::get('tim-kiem',[
		'uses' => 'SearchController@getSearch',
		'as' => 'page.index.search'
	]);
	Route::get('tim-san-pham',[
		'uses' => 'SearchController@getSearchPro',
		'as' => 'page.index.searchPro'
	]);
	Route::get('tim-gia',[
		'uses' => 'SearchController@getSearchPrice',
		'as' => 'page.index.searchPrice'
	]);
	Route::get('tim-san-pham-giam',[
		'uses' => 'SearchController@getSearchSale',
		'as' => 'page.index.searchSale'
	]);
	
	Route::get('gio-hang',[
		'uses' => 'CartController@getcart',
		'as' => 'page.index.cart'
	]);
	Route::get('them-vao-gio-hang',[
		'uses' => 'CartController@addItem',
		'as' => 'page.index.shopping'
	]);
	Route::get('delete',[
		'uses' =>'CartController@delete',
		'as' => 'page.index.delete'
	]);
	Route::get('update',[
		'uses' =>'CartController@update',
		'as' => 'page.index.update'
	]);

	Route::get('mua-hang',[
		'uses' =>'CheckoutController@getToCheckout',
		'as' => 'page.index.tocheckout'
	]);

	Route::post('mua-hang',[
		'uses' =>'CheckoutController@postToCheckout',
		'as' => 'page.index.tocheckout'
	]);

	Route::get('cam-on',[
		'uses' => 'CheckoutController@thanks',
		'as' => 'page.index.thanks'
	]);

	Route::get('lich-su-mua-hang',[
		'uses' => 'HistoryController@index',
		'as' =>'page.index.history'
	]);

});

 // Route::group(['namespace' =>'Sell'], function(){

 // 	Route::get()
 // });
Route::get('yeu-cau-truy-cap',function(){
	return view('admin.index.loi');
})->name('loi');