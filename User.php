  <?php

namespace App;

// use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    // use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table ='users';
    protected $primaryKey = 'id';
    public $timestamps=true;
    protected $fillable= [
        'username', 'email', 'password','fullname',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getItems()
   {
    return $this->orderBy('id','DESC')->paginate(5);
   }
   public function getItem($id){
    return $this->findOrFail($id);
   }

    public function cusDetail()
  {
    return $this->belongsTo('App\CusDetail', 'id_user');
  }

  public function getId($id){
    return $this->find($id);
   }

   public function getUser($id){

    return $this->join('bills','bills.id_user','=','users.id')
      ->select('bills.*', 'users.fullname','users.id as id_user')
      ->where('bills.id',$id)
      ->latest('id')->first();
    
  }

    public function getIdType($id){
    return $this->where('id_cat', $id)->get();
   }


   public function addItem($arItem){
    return $this->insert($arItem);
   }

   public function editItems($id, $arItem){
    $oItem = $this->findOrFail($id);
    return $oItem->update($arItem);
   }

   public function deleteItem($id){
    $oItem = $this->findOrFail($id);
    return $oItem->delete();
   }
}
